/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


import br.com.amazontrip.model.Escala;
import br.com.amazontrip.model.Cidade;
import br.com.amazontrip.dao.CidadeDao;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author n0mercy
 */
public class NewClass {

    public static void main(String[] args) {
        List<Cidade> list = new ArrayList<Cidade>();
        List<Escala> escalas = new ArrayList<Escala>();
        list = new CidadeDao().listar(Cidade.class);
        for(Cidade cidade: list){
            System.out.print("Origem: "+cidade.getTxNome()+"\n");
            escalas.addAll(cidade.getEscalas());
            for(Escala e: escalas){
                System.out.println("\tEscala: "+e.getId()+" -> "+e.getCidade().getTxNome());
            }
        }
    }
}
