/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.util;

/**
 *
 * @author n0mercy
 */

//classe que define os dias da semana, somente os valores inteiros são persistidos no Banco
public enum SemanaEnum {
    DOMINGO(1),SEGUNDA(2),TERÇA(3),QUARTA(4),QUINTA(5),SEXTA(6),SÁBADO(7);

    private final int valor;
    
    private SemanaEnum(int opcao) {
        this.valor = opcao;
    }
    
    public int getValor(){
        return valor;
    }
    
    public static String getValor(char param){
        String tag = null;
        for(SemanaEnum v: SemanaEnum.values()){
            if(Integer.parseInt(String.valueOf(param)) == v.getValor()){
                tag = v.name();
                break;
            }
        }
        return tag;
    }
    
    
}
