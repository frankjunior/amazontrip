/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.util;

import br.com.amazontrip.model.Passagem;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author n0mercy
 */
public class RelatorioUtil {
    
    public static void gerarRelatorio(List<Passagem> list, Map args) throws JRException{
        
        InputStream fonte = RelatorioUtil.class.getResourceAsStream("/br/com/amazontrip/relatorio/relatorioPassagem.jrxml");        
        JasperReport report = JasperCompileManager.compileReport(fonte);
        JasperPrint print = JasperFillManager.fillReport(report, args, new JRBeanCollectionDataSource(list));
        //JasperViewer.viewReport(print,false);
    }
    
}
