/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.CidadeDao;
import br.com.amazontrip.dao.CronogramaDao;
import br.com.amazontrip.dao.EmbarcacaoDao;
import br.com.amazontrip.model.Cidade;
import br.com.amazontrip.model.Cronograma;
import br.com.amazontrip.model.Embarcacao;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerCronograma", urlPatterns = {"/ControllerCronograma"})
public class ControllerCronograma extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerCronograma</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerCronograma at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pagina = "";
        String paginaRetorno = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Cronograma cronograma;
        Embarcacao embarcacao;
        Cidade cidadeOrigem;
        Cidade cidadeDestino;
        StringBuilder sbDescricao = new StringBuilder();
        if (request.getParameter("page") != null) {
            pagina = request.getParameter("page");
        }
        switch (pagina) {
            case "cadastrarCronograma"://ação de cadastro
                //criando um cronograma
                cronograma = new Cronograma();
                cidadeOrigem = new CidadeDao().buscarPorId(Integer.parseInt(request.getParameter("cidade_origem")));
                cidadeDestino = new CidadeDao().buscarPorId(Integer.parseInt(request.getParameter("cidade_destino")));
                embarcacao = new EmbarcacaoDao().buscarPorId(Integer.parseInt(request.getParameter("embarcacao")));
                sbDescricao.append(cidadeOrigem.getTxNome());
                sbDescricao.append(" - ");
                sbDescricao.append(cidadeDestino.getTxNome());
                cronograma.setTxDescricao(sbDescricao.toString());
                cronograma.setCidadeByIdCidadeOrigem(cidadeOrigem);
                cronograma.setCidadeByIdCidadeDestino(cidadeDestino);
                cronograma.setEmbarcacao(embarcacao);
                cronograma.setNbDiaSemanaChegada(request.getParameter("dia_chegada").charAt(0));
                cronograma.setNbDiaSemanaSaida(request.getParameter("dia_saida").charAt(0));
                 {
                    try {
                        cronograma.setNbHorarioChegada(sdf.parse(request.getParameter("hora_chegada")));
                        cronograma.setNbHorarioSaida(sdf.parse(request.getParameter("hora_saida")));
                    } catch (ParseException ex) {
                        Logger.getLogger(ControllerCronograma.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                cronograma.setTxLocalPartida(request.getParameter("local_partida"));
                new CronogramaDao().salvar(cronograma);
                paginaRetorno = "listandoCronograma.jsp";
                break;
            case "alterarCronograma"://ação de alteração
                //alterando um cronograma
                cronograma = new Cronograma();
                if (request.getParameter("id_cronograma") != null) {
                    cronograma = new CronogramaDao().buscarPorId(Integer.parseInt(request.getParameter("id_cronograma")));
                    cidadeOrigem = new CidadeDao().buscarPorId(Integer.parseInt(request.getParameter("cidade_origem")));
                    cidadeDestino = new CidadeDao().buscarPorId(Integer.parseInt(request.getParameter("cidade_destino")));
                    embarcacao = new EmbarcacaoDao().buscarPorId(Integer.parseInt(request.getParameter("embarcacao")));
                    sbDescricao.append(cidadeOrigem.getTxNome());
                    sbDescricao.append(" - ");
                    sbDescricao.append(cidadeDestino.getTxNome());
                    cronograma.setTxDescricao(sbDescricao.toString());
                    cronograma.setCidadeByIdCidadeOrigem(cidadeOrigem);
                    cronograma.setCidadeByIdCidadeDestino(cidadeDestino);
                    cronograma.setEmbarcacao(embarcacao);
                    System.out.println("P1: "+request.getParameter("dia_chegada").charAt(0));
                    System.out.println("P2: "+request.getParameter("dia_saida").charAt(0));
                    cronograma.setNbDiaSemanaChegada(request.getParameter("dia_chegada").charAt(0));
                    cronograma.setNbDiaSemanaSaida(request.getParameter("dia_saida").charAt(0));
                    {
                        try {
                            cronograma.setNbHorarioChegada(sdf.parse(request.getParameter("hora_chegada")));
                            cronograma.setNbHorarioSaida(sdf.parse(request.getParameter("hora_saida")));
                        } catch (ParseException ex) {
                            Logger.getLogger(ControllerCronograma.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    cronograma.setTxLocalPartida(request.getParameter("local_partida"));
                    new CronogramaDao().salvar(cronograma);
                    System.out.println("P3: "+cronograma.getNbDiaSemanaSaida());
                    paginaRetorno = "alterarCronograma.jsp";
                    request.getSession().setAttribute("entity", cronograma);
                }
                break;
        }
        response.sendRedirect("paginas/"+paginaRetorno);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
