/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.EmpresaDao;
import br.com.amazontrip.dao.FuncionarioDao;
import br.com.amazontrip.dao.TipoUsuarioDao;
import br.com.amazontrip.dao.UsuarioDao;
import br.com.amazontrip.model.Cliente;
import br.com.amazontrip.model.Empresa;
import br.com.amazontrip.model.Funcionario;
import br.com.amazontrip.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerFuncionario", urlPatterns = {"/ControllerFuncionario"})
public class ControllerFuncionario extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerFuncionario</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerFuncionario at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Funcionario funcionario;
        String page = "";
        String returnPage = "";
        String action = "";
        boolean key = false;
        if(request.getParameter("Action") != null){
            action = request.getParameter("Action");
            if(action.equals("ativar"))//verifica status atual do obj
                key = true;
            else
                key = false;
        }
        if (request.getParameter("page") != null) {
            page = request.getParameter("page");
            switch (page) {
                case "listandoFuncionario":
                    returnPage = "listandoFuncionario.jsp";
                    funcionario = new FuncionarioDao().buscarPorId(Integer.parseInt(request.getParameter("Id")));
                    funcionario.setBoStatus(key);//atualiza o obj
                    new FuncionarioDao().salvar(funcionario);
                    break;
            }
        }
        response.sendRedirect("paginas/" + returnPage);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Usuario usuario;
        Funcionario funcionario;
        String pagina = "";
        String paginaRetorno = "";
        if (request.getParameter("page") != null) {
            pagina = request.getParameter("page");
        }

        switch (pagina) {
            case "cadastrarFuncionario"://ação de cadastro
                //criando um usuário
                usuario = new Usuario();
                funcionario = new Funcionario();
                usuario.setTxLogin(request.getParameter("login"));
                usuario.setTxSenha(request.getParameter("senha"));
                usuario.setTipoUsuario(new TipoUsuarioDao().buscarPorId(3));
                new UsuarioDao().salvar(usuario);

                funcionario.setUsuario(usuario);
                funcionario.setEmpresa(new EmpresaDao().buscarPorId(Integer.parseInt(request.getParameter("empresa"))));
                funcionario.setTxNome(request.getParameter("nome"));
                funcionario.setTxCpf(request.getParameter("cpf"));
                new FuncionarioDao().salvar(funcionario);
                paginaRetorno = "cadastrarFuncionario.jsp";
                break;

            case "alterarFuncionario"://ação de alteração
                //criando um usuário
                usuario = new Usuario();
                funcionario = new Funcionario();

                if (request.getParameter("id_funcionario") != null) {
                    funcionario = new FuncionarioDao().buscarPorId(Integer.parseInt(request.getParameter("id_funcionario")));
                    usuario = new UsuarioDao().buscarPorId(funcionario.getUsuario().getId());

                    usuario.setTxLogin(request.getParameter("login"));
                    usuario.setTxSenha(request.getParameter("senha"));
                    usuario.setTipoUsuario(new TipoUsuarioDao().buscarPorId(4));
                    new UsuarioDao().salvar(usuario);

                    funcionario.setUsuario(usuario);
                    funcionario.setEmpresa(new EmpresaDao().buscarPorId(Integer.parseInt(request.getParameter("empresa"))));
                    funcionario.setTxNome(request.getParameter("nome"));
                    funcionario.setTxCpf(request.getParameter("cpf"));
                    new FuncionarioDao().salvar(funcionario);
                    request.getSession().setAttribute("entity", funcionario);
                    paginaRetorno = "alterarFuncionario.jsp";
                }
                break;
        }

        response.sendRedirect("paginas/"+paginaRetorno);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
