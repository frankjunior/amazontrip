/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.ClienteDao;
import br.com.amazontrip.dao.TipoClienteDao;
import br.com.amazontrip.dao.TipoUsuarioDao;
import br.com.amazontrip.dao.UsuarioDao;
import br.com.amazontrip.model.Cliente;
import br.com.amazontrip.model.TipoCliente;
import br.com.amazontrip.model.TipoUsuario;
import br.com.amazontrip.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerCliente", urlPatterns = {"/ControllerCliente"})
public class ControllerCliente extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerCliente</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerCliente at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Cliente cliente;
        String page = "";
        String returnPage = "";
        String action = "";
        boolean key = false;
        if(request.getParameter("Action") != null){//recebe a ação da tela JSP: ativar / desativar
            action = request.getParameter("Action");
            if(action.equals("ativar"))
                key = true;
            else
                key = false;
        }
        if (request.getParameter("page") != null) {//chega qual págica fez a requisição
            page = request.getParameter("page");
            switch (page) {//verificando o tipo da ação por meio da pagina
                case "listandoCliente"://requisição de ativar / desativar
                    returnPage = "listandoCliente.jsp";
                    cliente = new ClienteDao().buscarPorId(Integer.parseInt(request.getParameter("Id")));
                    cliente.setBoStatus(key);
                    new ClienteDao().salvar(cliente);
                    break;
                case "alterarCliente"://requisição de alteração dos dados
                    returnPage = "alterarCliente.jsp";
                    cliente = new ClienteDao().buscarPorId(Integer.parseInt(request.getParameter("Id")));
                    request.getSession().setAttribute("entity", cliente);//retorna o obj modificado para a tela de alteração
                    cliente.setBoStatus(key);
                    new ClienteDao().salvar(cliente);
                    break;
            }
        }
        response.sendRedirect("paginas/" + returnPage);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Cliente cliente;
        Usuario usuario;
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        String pagina = "";
        String paginaRetorno = "";
        if (request.getParameter("page") != null) {
            pagina = request.getParameter("page");//recebe a página que fez a requisição
        }

        switch (pagina) {
            case "cadastrarCliente"://ação de cadastro
                //criando um usuário
                usuario = new Usuario();
                cliente = new Cliente();
                usuario.setTxLogin(request.getParameter("login"));
                usuario.setTxSenha(request.getParameter("senha"));
                usuario.setTipoUsuario(new TipoUsuarioDao().buscarPorId(4));
                new UsuarioDao().salvar(usuario);

                cliente.setTxNome(request.getParameter("nome"));
                cliente.setTxCpf(request.getParameter("cpf"));
                cliente.setTxRg(request.getParameter("rg"));
                cliente.setTxTelefonePessoal(request.getParameter("telefonePessoal"));
                cliente.setTxTelefoneEmergencial(request.getParameter("telefoneEmergencial"));
                cliente.setUsuario(usuario);
                 {
                    try {
                        cliente.setDtNascimento(sdf.parse(request.getParameter("dataNascimento")));
                    } catch (ParseException ex) {
                        System.out.println("br.com.amazontrip.controller.ControllerCliente.doPost(): " + ex.getMessage());
                    }
                }
                cliente.setTipoCliente(new TipoClienteDao().buscarPorId(Integer.parseInt(request.getParameter("tipoCliente"))));
                new ClienteDao().salvar(cliente);

                paginaRetorno = "listandoCliente.jsp";
                break;
            case "alterarCliente"://ação de alteração
                //alterando um usuário
                usuario = new Usuario();
                cliente = new Cliente();
                if (request.getParameter("id_cliente") != null) {
                    cliente = new ClienteDao().buscarPorId(Integer.parseInt(request.getParameter("id_cliente")));
                    usuario = new UsuarioDao().buscarPorId(cliente.getUsuario().getId());

                    usuario.setTxLogin(request.getParameter("login"));
                    usuario.setTxSenha(request.getParameter("senha"));
                    new UsuarioDao().salvar(usuario);

                    cliente.setTxNome(request.getParameter("nome"));
                    cliente.setTxCpf(request.getParameter("cpf"));
                    cliente.setTxRg(request.getParameter("rg"));
                    cliente.setTxTelefonePessoal(request.getParameter("telefonePessoal"));
                    cliente.setTxTelefoneEmergencial(request.getParameter("telefoneEmergencial"));
                    cliente.setUsuario(usuario);
                    {
                        try {
                            cliente.setDtNascimento(sdf.parse(request.getParameter("dataNascimento")));
                        } catch (ParseException ex) {
                            System.out.println("br.com.amazontrip.controller.ControllerCliente.doPost(): " + ex.getMessage());
                        }
                    }
                    cliente.setTipoCliente(new TipoClienteDao().buscarPorId(Integer.parseInt(request.getParameter("tipoCliente"))));
                    new ClienteDao().salvar(cliente);
                    request.getSession().setAttribute("entity", cliente);
                    paginaRetorno = "alterarCliente.jsp";
                }
                break;
        }

        response.sendRedirect("paginas/" + paginaRetorno);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
