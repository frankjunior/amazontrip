/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.UsuarioDao;
import br.com.amazontrip.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerLogin", urlPatterns = {"/ControllerLogin"})
public class ControllerLogin extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerLogin</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerLogin at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        ////ação de LogOut pelo Botão Sair
        if(request.getParameter("action") != null){
            String action = request.getParameter("action");
            if(action.equals("logoff")){
                if(request.getSession().getAttribute("userEntity") != null)
                    request.getSession().invalidate();
                else
                    System.out.println("Sem usuário logado.");
            }            
        }
        response.sendRedirect("index.jsp");
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Usuario usuario = new Usuario();
        String login = null;
        String senha = null;
        login = request.getParameter("login");//recebe o login da tela
        senha = request.getParameter("senha");//recebe a senha da tela
        if (login != null && senha != null) {
            usuario = new UsuarioDao().efetuarLogin(login, senha);//verifica login e senha no banco e retorna o usuário, caso encontre.
            if (usuario != null) {//encontrou o usuário e foi feito o login com sucesso
                request.getSession().setAttribute("userEntity", usuario);//seta o obj usuário na sessão com a chave userEntity
                request.getSession().setAttribute("tipoUser", usuario.getTipoUsuario().getTxNome());//seta o tipo de usuário na sessão com a chave tipoUser
                /* init session */
                    HttpSession session = request.getSession();
                    session.setAttribute("user", usuario);//seta o obj usuário na sessão com a chave user
                    //setting session to expiry in 30 mins
                    session.setMaxInactiveInterval(30 * 60);
                    Cookie userName = new Cookie("user", usuario.getTxLogin());
                    userName.setMaxAge(30 * 60);
                    response.addCookie(userName);
                    response.sendRedirect("index.jsp");
            }else {//não encontrou o usuário
                System.out.println("Usuário ou senha inválida!");
                request.getSession().setAttribute("msg", "Usuario ou senha invalida!");
                response.sendRedirect("login.jsp?error");
            }
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
