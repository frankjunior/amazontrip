/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.EmbarcacaoDao;
import br.com.amazontrip.dao.EmpresaDao;
import br.com.amazontrip.dao.TipoEmbarcacaoDao;
import br.com.amazontrip.model.Embarcacao;
import br.com.amazontrip.model.Empresa;
import br.com.amazontrip.model.TipoEmbarcacao;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerEmbarcacao", urlPatterns = {"/ControllerEmbarcacao"})
public class ControllerEmbarcacao extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerEmbarcacao</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerEmbarcacao at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Embarcacao embarcacao;
        String page = "";
        String returnPage = "";
        String action = "";
        boolean key = false;
        if(request.getParameter("Action") != null){
            action = request.getParameter("Action");
            if(action.equals("ativar"))//verifica qual o atual status do obj
                key = true;
            else
                key = false;
        }
        if (request.getParameter("page") != null) {
            page = request.getParameter("page");
            switch (page) {
                case "listandoEmbarcacao":
                    returnPage = "listandoEmbarcacao.jsp";
                    embarcacao = new EmbarcacaoDao().buscarPorId(Integer.parseInt(request.getParameter("Id")));
                    embarcacao.setBoStatus(key);//atualiza o status
                    new EmbarcacaoDao().salvar(embarcacao);
                    break;
            }
        }
        response.sendRedirect("paginas/" + returnPage);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pagina = "";
        String paginaRetorno = "";
        Embarcacao embarcacao;
        TipoEmbarcacao tipoEmbarcacao;
        Empresa empresa;
        if (request.getParameter("page") != null) {
            pagina = request.getParameter("page");
        }
        switch (pagina) {
            case "cadastrarEmbarcacao"://ação de cadastro
                embarcacao = new Embarcacao();
                empresa = new EmpresaDao().buscarPorId(Integer.parseInt(request.getParameter("empresa")));
                tipoEmbarcacao = new TipoEmbarcacaoDao().buscarPorId(Integer.parseInt(request.getParameter("tipoEmbarcacao")));
                embarcacao.setEmpresa(empresa);
                embarcacao.setTipoEmbarcacao(tipoEmbarcacao);
                embarcacao.setBoAreaLazer(Boolean.valueOf(request.getParameter("areaLazer")));
                embarcacao.setBoClimatizacao(Boolean.valueOf(request.getParameter("climatizacao")));
                embarcacao.setBoTv(Boolean.valueOf(request.getParameter("tv")));
                embarcacao.setNbCapacidade(Integer.parseInt(request.getParameter("capacidade")));
                embarcacao.setNbInscricaoCapitania(Integer.parseInt(request.getParameter("inscricao")));
                embarcacao.setNbQuantidadeCamarote(Integer.parseInt(request.getParameter("camarote")));
                embarcacao.setTxContato(request.getParameter("contato"));
                embarcacao.setTxNome(request.getParameter("nomeEmbarcacao"));
                new EmbarcacaoDao().salvar(embarcacao);
                paginaRetorno = "listandoEmbarcacao.jsp";
                break;
                
            case "alterarEmbarcacao"://ação de alteração
                System.out.println("Chegou");
                embarcacao = new Embarcacao();
                if (request.getParameter("id_embarcacao") != null) {
                    embarcacao = new EmbarcacaoDao().buscarPorId(Integer.parseInt(request.getParameter("id_embarcacao")));
                    empresa = new EmpresaDao().buscarPorId(Integer.parseInt(request.getParameter("empresa")));
                    tipoEmbarcacao = new TipoEmbarcacaoDao().buscarPorId(Integer.parseInt(request.getParameter("tipoEmbarcacao")));

                    embarcacao.setEmpresa(empresa);
                    embarcacao.setTipoEmbarcacao(tipoEmbarcacao);
                    embarcacao.setBoAreaLazer(Boolean.valueOf(request.getParameter("areaLazer")));
                    embarcacao.setBoClimatizacao(Boolean.valueOf(request.getParameter("climatizacao")));
                    embarcacao.setBoTv(Boolean.valueOf(request.getParameter("tv")));
                    embarcacao.setNbCapacidade(Integer.parseInt(request.getParameter("capacidade")));
                    embarcacao.setNbInscricaoCapitania(Integer.parseInt(request.getParameter("inscricao")));
                    embarcacao.setNbQuantidadeCamarote(Integer.parseInt(request.getParameter("camarote")));
                    embarcacao.setTxContato(request.getParameter("contato"));
                    embarcacao.setTxNome(request.getParameter("nomeEmbarcacao"));
                    new EmbarcacaoDao().salvar(embarcacao);
                    paginaRetorno = "listandoEmbarcacao.jsp";                    
                    request.getSession().setAttribute("entity", embarcacao);
                }
                break;
        }
        response.sendRedirect("paginas/"+paginaRetorno);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
