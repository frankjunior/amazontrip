/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.CidadeDao;
import br.com.amazontrip.dao.CronogramaDao;
import br.com.amazontrip.dao.EscalaDao;
import br.com.amazontrip.model.Cidade;
import br.com.amazontrip.model.Cronograma;
import br.com.amazontrip.model.Escala;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerEscala", urlPatterns = {"/ControllerEscala"})
public class ControllerEscala extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerEscala</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerEscala at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pagina = "";
        String paginaRetorno = "";
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
        Escala escala;
        Cronograma cronograma;
        Cidade cidade;
        if (request.getParameter("page") != null) {
            pagina = request.getParameter("page");
        }
        switch (pagina) {
            case "cadastrarEscala"://ação de cadastro
                //adicionando escala
                escala = new Escala();
                cronograma = new CronogramaDao().buscarPorId(Integer.parseInt(request.getParameter("cronograma")));
                cidade = new CidadeDao().buscarPorId(Integer.parseInt(request.getParameter("escala")));
                escala.setCronograma(cronograma);
                escala.setCidade(cidade);
                double camarote = Double.parseDouble(request.getParameter("valor_camarote").replace(",", "."));
                double padrao = Double.parseDouble(request.getParameter("valor_padrao").replace(",", "."));
                escala.setNbValorCamarote(BigDecimal.valueOf(camarote));
                escala.setNbValorPadrao(BigDecimal.valueOf(padrao));
                escala.setTxDia(request.getParameter("dia_semana").charAt(0));
                 {
                    try {
                        escala.setTxHorario(sdf.parse(request.getParameter("data_hora")));
                    } catch (ParseException ex) {
                        Logger.getLogger(ControllerEscala.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
                new EscalaDao().salvar(escala);
                paginaRetorno = "listandoEscala.jsp";
                break;

            case "alterarEscala"://ação de alteração
                //alterando escala
                escala = new Escala();
                if (request.getParameter("id_escala") != null) {
                    escala = new EscalaDao().buscarPorId(Integer.parseInt(request.getParameter("id_escala")));
                    cronograma = new CronogramaDao().buscarPorId(Integer.parseInt(request.getParameter("cronograma")));
                    cidade = new CidadeDao().buscarPorId(Integer.parseInt(request.getParameter("escala")));
                    escala.setCronograma(cronograma);
                    escala.setCidade(cidade);
                    camarote = Double.parseDouble(request.getParameter("valor_camarote").replace(",", "."));
                    padrao = Double.parseDouble(request.getParameter("valor_padrao").replace(",", "."));
                    escala.setNbValorCamarote(BigDecimal.valueOf(camarote));
                    escala.setNbValorPadrao(BigDecimal.valueOf(padrao));
                    escala.setTxDia(request.getParameter("dia_semana").charAt(0));
                    {
                        try {
                            escala.setTxHorario(sdf.parse(request.getParameter("data_hora")));
                        } catch (ParseException ex) {
                            Logger.getLogger(ControllerEscala.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    }
                    new EscalaDao().salvar(escala);
                    request.getSession().setAttribute("entity", escala);
                    paginaRetorno = "listandoEscala.jsp";
                }
                break;
        }
        response.sendRedirect("paginas/"+paginaRetorno);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
