/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.ClienteDao;
import br.com.amazontrip.dao.EscalaDao;
import br.com.amazontrip.dao.FuncionarioDao;
import br.com.amazontrip.dao.PassagemDao;
import br.com.amazontrip.model.Cliente;
import br.com.amazontrip.model.Embarcacao;
import br.com.amazontrip.model.Escala;
import br.com.amazontrip.model.Funcionario;
import br.com.amazontrip.model.Passagem;
import br.com.amazontrip.model.Usuario;
import br.com.amazontrip.util.RelatorioUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerPassagem", urlPatterns = {"/ControllerPassagem"})
public class ControllerPassagem extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerPassagem</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerPassagem at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pagina = "";
        String paginaRetorno = "";
        Escala escala;
        Embarcacao embarcacao;
        Cliente cliente = null;
        ServletOutputStream outputStream = null;
        Usuario usuario;
        Funcionario funcionario;
        List<Passagem> list = new ArrayList<Passagem>();
        Passagem passagem = null;
        StringBuilder sbDescricao = new StringBuilder();
        if (request.getParameter("page") != null) {
            pagina = request.getParameter("page");
        }
        switch (pagina) {
            case "compraPassagem"://efetua a compra da passagem
                if (request.getParameter("escala") != null) {
                    passagem = new Passagem();
                    escala = new EscalaDao().buscarPorId(Integer.parseInt(request.getParameter("escala")));//busca a escala que veio da tela compraPassagem.jsp
                    if (request.getParameter("id_cliente") != null) //verifica se o cliente que está comprando
                    {
                        cliente = new ClienteDao().buscarPorId(Integer.parseInt(request.getParameter("id_cliente")));//busca o cliente
                    }
                    //verifica se há funcionário vindo da tela de compra de passagem
                    if (request.getParameter("id_funcionario") != null) {//verifica o funcionário, caso ele esteja logado
                        funcionario = new FuncionarioDao().buscarPorId(Integer.parseInt(request.getParameter("id_funcionario")));//busca o funcionário
                    } else {
                        funcionario = new FuncionarioDao().buscarPorId(1);//se não houver funcionário na compra, seta um funcionário padrão com ID = 1; (necessita estar cadastrado antes de efetuar uma venda sem funcionário)
                    }
                    //verifica o tipo de passagem selecionada na compra
                    if (request.getParameter("tipo_passagem") != null) {//verifica o tipo de passagem para determinar o seu valor 1 = padrão 2 = camarote
                        if (Integer.parseInt(request.getParameter("tipo_passagem")) == 1) {

                            if (cliente != null && cliente.getTipoCliente().getId() == 1) {
                                passagem.setNbValor(escala.getNbValorPadrao());
                            } else if (cliente != null && cliente.getTipoCliente().getId() == 2) {
                                passagem.setNbValor(escala.getNbValorPadrao().divide(new BigDecimal(2)));
                            }

                        } else if (Integer.parseInt(request.getParameter("tipo_passagem")) == 2) {

                            if (cliente != null && cliente.getTipoCliente().getId() == 1) {
                                passagem.setNbValor(escala.getNbValorCamarote());
                            } else if (cliente != null && cliente.getTipoCliente().getId() == 2) {
                                passagem.setNbValor(escala.getNbValorCamarote().divide(new BigDecimal(2)));
                            }

                        }
                    }

                    embarcacao = escala.getCronograma().getEmbarcacao();
                    passagem.setEmbarcacao(embarcacao);
                    passagem.setFuncionario(funcionario);

                    passagem.setCliente(cliente);
                    passagem.setEscala(escala);
                    passagem.setDtEmissao(new Date());
                    passagem.setTxDiaSaida(escala.getTxDia());
                    passagem.setTxHorarioSaida(escala.getTxHorario());
                    new PassagemDao().salvar(passagem);
                    //imprimindo
                    list = new ArrayList<Passagem>();
                    list.add(passagem);
                    InputStream fonte2 = RelatorioUtil.class.getResourceAsStream("/br/com/amazontrip/relatorio/passagem.jrxml");//seleciona o arquivo de design do relatório
                    JasperReport report2;

                    
                    byte[] bytes2 = null;
                    try {

                        report2 = JasperCompileManager.compileReport(fonte2);
                        JasperPrint print2 = JasperFillManager.fillReport(report2, null, new JRBeanCollectionDataSource(list));
                        bytes2 = JasperExportManager.exportReportToPdf(print2);//exporta em bytes o pdf

                    } catch (JRException ex) {
                        Logger.getLogger(RelatorioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    
                    response.setHeader("Content-Disposition", "inline; filename=\"passagem.pdf\"");
                    response.setContentType("application/pdf");
                    response.setContentLength(bytes2.length);
                    outputStream = response.getOutputStream();
                    outputStream.write(bytes2, 0, bytes2.length);
                    outputStream.flush();
                    outputStream.close();
                    paginaRetorno = "../index.jsp";
                }
                break;
        }
        

    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
