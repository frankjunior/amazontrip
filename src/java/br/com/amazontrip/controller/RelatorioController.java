/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.PassagemDao;
import br.com.amazontrip.model.Passagem;
import br.com.amazontrip.util.RelatorioUtil;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "RelatorioController", urlPatterns = {"/RelatorioController"})
public class RelatorioController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RelatorioServlet</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RelatorioServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Passagem> list = new ArrayList<Passagem>();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        Date inicio = null;
        Date fim = null;
        String page = null;
        if (request.getParameter("pagina") != null) {
            page = request.getParameter("pagina");

            switch (page) {
                case "gerarRelatorio":
                    if (Integer.parseInt(request.getParameter("tipo")) == 1) {//monta a lista sem parametros de data
                        list = new PassagemDao().listar(Passagem.class);
                    } else {//monta a lista com parametros de data
                        try {
                            inicio = sdf.parse(request.getParameter("data_inicio"));
                            fim = sdf.parse(request.getParameter("data_fim"));
                            list = new PassagemDao().listarPorData(inicio, fim);
                        } catch (ParseException ex) {
                            Logger.getLogger(RelatorioController.class.getName()).log(Level.SEVERE, null, ex);
                        }

                    }

                    InputStream fonte = RelatorioUtil.class.getResourceAsStream("/br/com/amazontrip/relatorio/relatorioPassagem.jrxml");//seleciona o arquivo de design do relatório
                    JasperReport report;
                    ServletOutputStream outputStream = null;
                    byte[] bytes = null;
                    try {

                        report = JasperCompileManager.compileReport(fonte);
                        JasperPrint print = JasperFillManager.fillReport(report, null, new JRBeanCollectionDataSource(list));
                        bytes = JasperExportManager.exportReportToPdf(print);//exporta em bytes o pdf

                    } catch (JRException ex) {
                        Logger.getLogger(RelatorioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    outputStream = response.getOutputStream();
                    if (Integer.parseInt(request.getParameter("tipo")) == 1)//tipo 1 relatorio geral
                    {
                        response.setHeader("Content-Disposition", "inline; filename=\"relatorio.pdf\"");
                    } else//tipo 2 relatorio com datas
                    {
                        response.setHeader("Content-Disposition", "inline; filename=\"relatorioPorPeriodo.pdf\"");
                    }

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes.length);
                    outputStream.write(bytes, 0, bytes.length);
                    outputStream.flush();
                    outputStream.close();
                    break;
                case "compraPassagem":
                    //imprimindo a passagem em pdf
                    list = new ArrayList<Passagem>();
                    list.add(new PassagemDao().buscarPorId(Integer.parseInt(request.getParameter("id_passagem"))));
                    InputStream fonte2 = RelatorioUtil.class.getResourceAsStream("/br/com/amazontrip/relatorio/passagem.jrxml");//seleciona o arquivo de design do relatório
                    JasperReport report2;

                    outputStream = response.getOutputStream();
                    byte[] bytes2 = null;
                    try {

                        report2 = JasperCompileManager.compileReport(fonte2);
                        JasperPrint print2 = JasperFillManager.fillReport(report2, null, new JRBeanCollectionDataSource(list));
                        bytes2 = JasperExportManager.exportReportToPdf(print2);//exporta em bytes o pdf

                    } catch (JRException ex) {
                        Logger.getLogger(RelatorioController.class.getName()).log(Level.SEVERE, null, ex);
                    }
                    response.setHeader("Content-Disposition", "inline; filename=\"passagem.pdf\"");

                    response.setContentType("application/pdf");
                    response.setContentLength(bytes2.length);
                    outputStream.write(bytes2, 0, bytes2.length);
                    outputStream.flush();
                    outputStream.close();
                    break;
            }
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
