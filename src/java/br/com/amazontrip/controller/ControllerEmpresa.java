/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.EmpresaDao;
import br.com.amazontrip.dao.TipoUsuarioDao;
import br.com.amazontrip.dao.UsuarioDao;
import br.com.amazontrip.model.Empresa;
import br.com.amazontrip.model.Usuario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "ControllerEmpresa", urlPatterns = {"/ControllerEmpresa"})
public class ControllerEmpresa extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet ControllerEmpresa</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet ControllerEmpresa at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        Empresa empresa;
        String page = "";
        String returnPage = "";
        String action = "";
        boolean key = false;
        if(request.getParameter("Action") != null){
            action = request.getParameter("Action");
            if(action.equals("ativar"))//verefica o status atual do obj
                key = true;
            else
                key = false;
        }
        if (request.getParameter("page") != null) {
            page = request.getParameter("page");
            switch (page) {
                case "listandoEmpresa":
                    returnPage = "listandoEmpresa.jsp";
                    empresa = new EmpresaDao().buscarPorId(Integer.parseInt(request.getParameter("Id")));
                    empresa.setBoStatus(key);//atualiza o status
                    new EmpresaDao().salvar(empresa);
                    break;
            }
        }
        response.sendRedirect("paginas/" + returnPage);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pagina = "";
        String paginaRetorno = "";
        Empresa empresa;
        Usuario usuario;
        if (request.getParameter("page") != null) {
            pagina = request.getParameter("page");
        }
        switch (pagina) {
            case "cadastrarEmpresa"://ação de cadastro
                //criando um usuário
                usuario = new Usuario();
                empresa = new Empresa();
                usuario.setTxLogin(request.getParameter("login"));
                usuario.setTxSenha(request.getParameter("senha"));
                usuario.setTipoUsuario(new TipoUsuarioDao().buscarPorId(2));
                new UsuarioDao().salvar(usuario);

                empresa.setTxCnpj(request.getParameter("cnpj"));
                empresa.setTxNome(request.getParameter("nome"));
                empresa.setTxProprietario(request.getParameter("proprietario"));
                empresa.setTxTelefone(request.getParameter("telefone"));
                empresa.setUsuario(usuario);
                new EmpresaDao().salvar(empresa);
                paginaRetorno = "listandoEmpresa.jsp";
                break;
            case "alterarEmpresa"://ação de alteração
                //criando um usuário
                usuario = new Usuario();
                empresa = new Empresa();
                if (request.getParameter("id_empresa") != null) {
                    empresa = new EmpresaDao().buscarPorId(Integer.parseInt(request.getParameter("id_empresa")));
                    usuario = new UsuarioDao().buscarPorId(empresa.getUsuario().getId());
                    usuario.setTxLogin(request.getParameter("login"));
                    usuario.setTxSenha(request.getParameter("senha"));
                    usuario.setTipoUsuario(new TipoUsuarioDao().buscarPorId(2));
                    new UsuarioDao().salvar(usuario);

                    empresa.setTxCnpj(request.getParameter("cnpj"));
                    empresa.setTxNome(request.getParameter("nome"));
                    empresa.setTxProprietario(request.getParameter("proprietario"));
                    empresa.setTxTelefone(request.getParameter("telefone"));
                    empresa.setUsuario(usuario);
                    new EmpresaDao().salvar(empresa);
                    paginaRetorno = "alterarEmpresa.jsp";
                    request.getSession().setAttribute("entity", empresa);
                }
                break;
        }

        response.sendRedirect("paginas/"+paginaRetorno);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
