/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.controller;

import br.com.amazontrip.dao.ClienteDao;
import br.com.amazontrip.dao.CronogramaDao;
import br.com.amazontrip.dao.EmbarcacaoDao;
import br.com.amazontrip.dao.EmpresaDao;
import br.com.amazontrip.dao.EscalaDao;
import br.com.amazontrip.dao.FuncionarioDao;
import br.com.amazontrip.model.Cliente;
import br.com.amazontrip.model.Cronograma;
import br.com.amazontrip.model.Embarcacao;
import br.com.amazontrip.model.Empresa;
import br.com.amazontrip.model.Escala;
import br.com.amazontrip.model.Funcionario;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author n0mercy
 */
@WebServlet(name = "RedirectServlet", urlPatterns = {"/RedirectServlet"})
public class RedirectServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet RedirectServlet</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet RedirectServlet at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pagina = "";
        String paginaRetorno = "";
        
        if(request.getParameter("pagina") != null)//verifica a tela que fez a requisição
            pagina = request.getParameter("pagina");
        
        switch(pagina){
            case "listandoCliente"://tela listandoCliente
                Cliente cliente = new Cliente();
                if(request.getParameter("action").equals("alterar")){
                    cliente = new ClienteDao().buscarPorId(Integer.parseInt(request.getParameter("id_cliente")));//recupera o obj pelo id
                    request.getSession().setAttribute("entity", cliente);//insere este obj na sessão e retorna para a pagina pertinente
                    paginaRetorno = "paginas/alterarCliente.jsp";
                }
                break;
            case "listandoFuncionario"://tela listandoFuncionario
                Funcionario funcionario = new Funcionario();
                if(request.getParameter("action").equals("alterar")){
                    funcionario = new FuncionarioDao().buscarPorId(Integer.parseInt(request.getParameter("id_funcionario")));//recupera o obj pelo id
                    request.getSession().setAttribute("entity", funcionario);//insere este obj na sessão e retorna para a pagina pertinente
                    paginaRetorno = "paginas/alterarFuncionario.jsp";
                }
                break;
            case "listandoEmpresa"://tela listandoEmpresa
                Empresa empresa = new Empresa();
                if(request.getParameter("action").equals("alterar")){
                    empresa = new EmpresaDao().buscarPorId(Integer.parseInt(request.getParameter("id_empresa")));//recupera o obj pelo id
                    request.getSession().setAttribute("entity", empresa);//insere este obj na sessão e retorna para a pagina pertinente
                    paginaRetorno = "paginas/alterarEmpresa.jsp";
                }
                break;
                
                
            case "listandoEmbarcacao"://tela listandoEmbarcacao
                Embarcacao embarcacao = new Embarcacao();
                if(request.getParameter("action").equals("alterar")){
                    embarcacao = new EmbarcacaoDao().buscarPorId(Integer.parseInt(request.getParameter("id_embarcacao")));//recupera o obj pelo id
                    request.getSession().setAttribute("entity", embarcacao);//insere este obj na sessão e retorna para a pagina pertinente
                    paginaRetorno = "paginas/alterarEmbarcacao.jsp";
                }
                break;
            case "listandoCronograma"://tela listandoCronograma
                Cronograma cronograma = new Cronograma();
                if(request.getParameter("action").equals("alterar")){
                    cronograma = new CronogramaDao().buscarPorId(Integer.parseInt(request.getParameter("id_cronograma")));//recupera o obj pelo id
                    request.getSession().setAttribute("entity", cronograma);//insere este obj na sessão e retorna para a pagina pertinente
                    paginaRetorno = "paginas/alterarCronograma.jsp";
                }
                break;
            case "listandoEscala"://tela listandoEscala
                Escala escala = new Escala();
                if(request.getParameter("action").equals("alterar")){
                    escala = new EscalaDao().buscarPorId(Integer.parseInt(request.getParameter("id_escala")));//recupera o obj pelo id
                    request.getSession().setAttribute("entity", escala);//insere este obj na sessão e retorna para a pagina pertinente
                    paginaRetorno = "paginas/alterarEscala.jsp";
                }
                break;
        }
        
        response.sendRedirect(paginaRetorno);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
