/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;

import br.com.amazontrip.model.Cidade;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class CidadeDao extends GenericDao{
    
    Session session;
    Transaction tx;
    List<Cidade> list;
    
    public Cidade salvar(Cidade cidade) throws HibernateException {
        super.saveOrUpdate(cidade);
        return cidade;
    }
    
    public Cidade buscarPorId(int id) throws HibernateException {
        return (Cidade) super.find(Cidade.class, id);
    }

    public List<Cidade> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(Cidade cidade) throws HibernateException {
        super.delete(cidade);
    }
       
}
