/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;

import br.com.amazontrip.model.Funcionario;
import br.com.amazontrip.model.Usuario;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class FuncionarioDao extends GenericDao{
    
    Session session;
    Transaction tx;
    List<Funcionario> list;
    public Funcionario salvar(Funcionario funcionario) throws HibernateException {
        super.saveOrUpdate(funcionario);
        return funcionario;
    }
    
    public Funcionario buscarPorId(int id) throws HibernateException {
        return (Funcionario) super.find(Funcionario.class, id);
    }

    public List<Funcionario> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(Funcionario funcionario) throws HibernateException {
        super.delete(funcionario);
    }
    
    public List<Funcionario> pesquisarPorNome(String busca){
        session = getSession();
        tx = session.beginTransaction();
        try{
            Query query = session.createQuery("from Funcionario f where f.txNome like :param ").setParameter("param", busca.concat("%"));
            list = query.list();
            tx.commit();            
        }catch(HibernateException e){           
            tx.rollback();
            System.out.println("br.com.fametro.dao.FuncionarioDao.pesquisarPorNome(): "+e.getMessage());
        }finally {
            if(session.isOpen())
                session.close();
        }
        return list;
    }
    
    public Funcionario funcionarioPeloUsuario(Usuario usuario){
        session = getSession();
        tx = session.beginTransaction();
        try{
            Query query = session.createQuery("from Funcionario f where f.usuario.id = :param ").setParameter("param", usuario.getId());
            list = query.list();
            tx.commit();            
        }catch(HibernateException e){           
            tx.rollback();
            System.out.println("br.com.amazontrip.dao.FuncionarioDao.funcionarioPeloUsuario(): "+e.getMessage());
        }finally {
            if(session.isOpen())
                session.close();
        }
        return list.get(0);
    }
}
