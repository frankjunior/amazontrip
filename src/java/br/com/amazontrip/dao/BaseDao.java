/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;

import br.com.amazontrip.connection.HibernateUtil;
import org.hibernate.Session;

/**
 *
 * @author n0mercy
 */
public class BaseDao {
    private static Session session;
    
    public static Session getSession(){
        session = HibernateUtil.getSessionFactory().openSession();
        if(session.isOpen())
            session = HibernateUtil.getSessionFactory().getCurrentSession();
        
        return session;
    }
}
