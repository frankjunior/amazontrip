/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;
import br.com.amazontrip.model.TipoUsuario;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author n0mercy
 */
public class TipoUsuarioDao extends GenericDao {

    public TipoUsuario salvar(TipoUsuario tipoUsuario) throws HibernateException {
        super.saveOrUpdate(tipoUsuario);
        return tipoUsuario;
    }
    
    public TipoUsuario buscarPorId(int id) throws HibernateException {
        return (TipoUsuario) super.find(TipoUsuario.class, id);
    }

    public List<TipoUsuario> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(TipoUsuario tipoUsuario) throws HibernateException {
        super.delete(tipoUsuario);
    }
}
