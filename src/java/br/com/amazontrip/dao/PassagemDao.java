/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;

import br.com.amazontrip.model.Escala;
import br.com.amazontrip.model.Passagem;
import java.util.Date;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class PassagemDao extends GenericDao{
    
    Session session;
    Transaction tx;
    List<Passagem> list;
    
    public Passagem salvar(Passagem passagem) throws HibernateException {
        super.saveOrUpdate(passagem);
        return passagem;
    }
    
    public Passagem buscarPorId(int id) throws HibernateException {
        return (Passagem) super.find(Passagem.class, id);
    }

    public List<Passagem> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(Passagem passagem) throws HibernateException {
        super.delete(passagem);
    }
    
    public List<Passagem> listarPorData(Date inicio, Date fim) {
        session = getSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Passagem p where p.dtEmissao between :from and :to")
                    .setParameter("from", inicio)
                    .setParameter("to", fim);
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            System.out.println("br.com.fametro.dao.EscalaDao.quantidadePassagemnsPorEscala: " + e.getMessage());
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
    public List<Passagem> quantidadePassagemnsPorEscala(Escala escala) {
        session = getSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Passagem p where p.escala.id = :esc and p.escala.cidade.id = :cid and p.escala.cronograma.cidadeByIdCidadeDestino.id = :cro and p.escala.cronograma.embarcacao.id = :emb")
                    .setParameter("cid", escala.getCidade().getId())
                    .setParameter("cro", escala.getCronograma().getCidadeByIdCidadeDestino().getId())
                    .setParameter("emb", escala.getCronograma().getEmbarcacao().getId())
                    .setParameter("esc", escala.getId());
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            System.out.println("br.com.fametro.dao.EscalaDao.quantidadePassagemnsPorEscala: " + e.getMessage());
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        return list;
    }
    
}
