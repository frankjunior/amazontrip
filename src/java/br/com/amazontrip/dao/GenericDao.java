/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;

import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public abstract class GenericDao extends BaseDao{
    private Session session;
    private Transaction tx;

    public GenericDao() {
        
    }

    protected Object saveOrUpdate(Object obj) {
        try {
            session = getSession();
            tx = session.beginTransaction();
            session.saveOrUpdate(obj);
            tx.commit();
        } catch (HibernateException e) {
            System.out.println("br.com.fametro.dao.GenericDao.saveOrUpdate(): "+e.getMessage());
        } finally {
            if(session.isOpen())
                session.close();
        }
        return obj;
    }

    protected void delete(Object obj) {
        try {
            session = getSession();
            tx = session.beginTransaction();
            session.delete(obj);
            tx.commit();
        } catch (HibernateException e) {
            System.out.println("br.com.fametro.dao.GenericDao.delete(): "+e.getMessage());
        } finally {
            if(session.isOpen())
                session.close();
        }
    }

    protected Object find(Class clazz, int id) {
        Object obj = null;
        try {
            session = getSession();
            tx = session.beginTransaction();
            obj = session.load(clazz, id);
            tx.commit();
        } catch (HibernateException e) {
            System.out.println("br.com.fametro.dao.GenericDao.find(): "+e.getMessage());
        } finally {
            if(session.isOpen())
                session.close();
        }
        return obj;
    }

    protected List findAll(Class clazz) {
        List objects = null;
        try {
            session = getSession();
            tx = session.beginTransaction();
            Query query = session.createQuery("from " + clazz.getName());
            objects = query.list();
            tx.commit();
        } catch (HibernateException e) {
            System.out.println("br.com.fametro.dao.GenericDao.findAll(): "+e.getMessage());
        } finally {
            if(session.isOpen())
                session.close();
        }
        return objects;
    }
}
