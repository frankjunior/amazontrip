/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;

import br.com.amazontrip.model.Embarcacao;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class EmbarcacaoDao extends  GenericDao{
    
    Session session;
    Transaction tx;
    List<Embarcacao> list;
    
    public Embarcacao salvar(Embarcacao embarcacao) throws HibernateException {
        super.saveOrUpdate(embarcacao);
        return embarcacao;
    }
    
    public Embarcacao buscarPorId(int id) throws HibernateException {
        return (Embarcacao) super.find(Embarcacao.class, id);
    }

    public List<Embarcacao> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(Embarcacao tipoEmbarcacao) throws HibernateException {
        super.delete(tipoEmbarcacao);
    }
    
    public List<Embarcacao> pesquisarPorNome(String busca){
        session = getSession();
        tx = session.beginTransaction();
        try{
            Query query = session.createQuery("from Embarcacao e where e.txNome like :param ").setParameter("param", busca.concat("%"));
            list = query.list();
            tx.commit();            
        }catch(HibernateException e){           
            tx.rollback();
            System.out.println("br.com.fametro.dao.EmbarcacaoDao.pesquisarPorNome(): "+e.getMessage());
        }finally {
            if(session.isOpen())
                session.close();
        }
        return list;
    }
    
}
