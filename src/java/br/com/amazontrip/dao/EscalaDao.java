/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;

import br.com.amazontrip.model.Cidade;
import br.com.amazontrip.model.Cronograma;
import br.com.amazontrip.model.Escala;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class EscalaDao extends GenericDao {

    Session session;
    Transaction tx;
    List<Escala> list;

    public Escala salvar(Escala cronograma) throws HibernateException {
        super.saveOrUpdate(cronograma);
        return cronograma;
    }

    public Escala buscarPorId(int id) throws HibernateException {
        return (Escala) super.find(Escala.class, id);
    }

    public List<Escala> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz);
    }

    public void delete(Escala escala) throws HibernateException {
        super.delete(escala);
    }

    public List<Escala> pesquisarPorCronograma(String busca) {
        session = getSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Escala e where e.cronograma.txDescricao like :param ").setParameter("param", busca.concat("%"));
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            System.out.println("br.com.fametro.dao.EscalaDao.pesquisarPorNome(): " + e.getMessage());
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public List<Escala> cidadesComEscalaOrigem() {
        session = getSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Escala e group by e.cidade.id");
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            System.out.println("br.com.fametro.dao.EscalaDao.pesquisarPorNome(): " + e.getMessage());
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        return list;
    }    

    public List<Escala> cidadesComEscala(Cidade cidade) {
        session = getSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Escala e where e.cidade.id = :cid")
                    .setParameter("cid", cidade.getId());
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            System.out.println("br.com.fametro.dao.EscalaDao.pesquisarPorNome(): " + e.getMessage());
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public List<Escala> getEscalaCronogramaCidade(int cidade, int cronograma) {
        session = getSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Escala e where e.cidade.id = :cid and e.cronograma.id = :cro")
                    .setParameter("cid", cidade)
                    .setParameter("cro", cronograma);
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            System.out.println("br.com.fametro.dao.EscalaDao.pesquisarPorNome(): " + e.getMessage());
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

    public List<Escala> cidadesComEscalaNoCronograma(Cronograma cronograma, Cidade cidade) {
        session = getSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Escala e where e.cronograma.id = :cro and e.cidade.id = :cid group by e.cronograma.id")
                    .setParameter("cro", cronograma.getId()).setParameter("cid", cidade.getId());
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            System.out.println("br.com.fametro.dao.EscalaDao.pesquisarPorNome(): " + e.getMessage());
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        return list;
    }

}
