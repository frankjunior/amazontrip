/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;
import br.com.amazontrip.model.Usuario;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class UsuarioDao extends GenericDao{

    Session session;
    Transaction tx;
    List<Usuario> list;
    
    public Usuario salvar(Usuario usuario) throws HibernateException {
        super.saveOrUpdate(usuario);
        return usuario;
    }
    
    public Usuario buscarPorId(int id) throws HibernateException {
        return (Usuario) super.find(Usuario.class, id);
    }

    public List<Usuario> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(Usuario usuario) throws HibernateException {
        super.delete(usuario);
    }
    
    public Usuario efetuarLogin(String login, String senha) {
        session = getSession();
        tx = session.beginTransaction();
        try {
            Query query = session.createQuery("from Usuario u where u.txLogin = :param1 and u.txSenha = :param2").setParameter("param1", login).setParameter("param2", senha);
            list = query.list();
            tx.commit();
        } catch (HibernateException e) {
            tx.rollback();
            System.out.println("br.com.fametro.dao.EscalaDao.pesquisarPorNome(): " + e.getMessage());
        } finally {
            if (session.isOpen()) {
                session.close();
            }
        }
        if(list.size() > 0)
            return list.get(0);
        else
            return null;
    }
    
}
