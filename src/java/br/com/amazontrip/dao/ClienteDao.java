/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;
import br.com.amazontrip.model.Cliente;
import br.com.amazontrip.model.Usuario;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class ClienteDao extends GenericDao{

    Session session;
    Transaction tx;
    List<Cliente> list;
    
    public Cliente salvar(Cliente cliente) throws HibernateException {
        super.saveOrUpdate(cliente);
        return cliente;
    }
    
    public Cliente buscarPorId(int id) throws HibernateException {
        return (Cliente) super.find(Cliente.class, id);
    }

    public List<Cliente> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(Cliente cliente) throws HibernateException {
        super.delete(cliente);
    }
    
    public List<Cliente> pesquisarPorNome(String busca){
        session = getSession();
        tx = session.beginTransaction();
        try{
            Query query = session.createQuery("from Cliente c where c.txNome like :param ").setParameter("param", busca.concat("%"));
            list = query.list();
            tx.commit();            
        }catch(HibernateException e){           
            tx.rollback();
            System.out.println("br.com.amazontrip.dao.ClienteDao.pesquisarPorNome(): "+e.getMessage());
        }finally {
            if(session.isOpen())
                session.close();
        }
        return list;
    }
    
    public Cliente buscarPeloCPF(String cpf){
        session = getSession();
        tx = session.beginTransaction();
        try{
            Query query = session.createQuery("from Cliente c where c.txCpf = :param ").setParameter("param", cpf);
            list = query.list();
            tx.commit();            
        }catch(HibernateException e){           
            tx.rollback();
            System.out.println("br.com.amazontrip.dao.ClienteDao.buscarPeloCPF(): "+e.getMessage());
        }finally {
            if(session.isOpen())
                session.close();
        }
        if(list.size() > 0)
            return list.get(0);
        else
            return null;
    }
    
    public Cliente clientePeloUsuario(Usuario usuario){
        session = getSession();
        tx = session.beginTransaction();
        try{
            Query query = session.createQuery("from Cliente c where c.usuario.id = :param ").setParameter("param", usuario.getId());
            list = query.list();
            tx.commit();            
        }catch(HibernateException e){           
            tx.rollback();
            System.out.println("br.com.amazontrip.dao.ClienteDao.clientePeloUsuario(): "+e.getMessage());
        }finally {
            if(session.isOpen())
                session.close();
        }
        return list.get(0);
    }
}
