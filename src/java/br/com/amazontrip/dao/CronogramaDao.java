/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;
import br.com.amazontrip.model.Cronograma;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class CronogramaDao extends GenericDao{
    Session session;
    Transaction tx;
    List<Cronograma> list;
    
    
    public Cronograma salvar(Cronograma cronograma) throws HibernateException {
        super.saveOrUpdate(cronograma);
        return cronograma;
    }
    
    public Cronograma buscarPorId(int id) throws HibernateException {
        return (Cronograma) super.find(Cronograma.class, id);
    }

    public List<Cronograma> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(Cronograma cronograma) throws HibernateException {
        super.delete(cronograma);
    }
    
    public List<Cronograma> pesquisarPorDescricao(String busca){
        session = getSession();
        tx = session.beginTransaction();
        try{
            Query query = session.createQuery("from Cronograma f where f.txDescricao like :param ").setParameter("param", busca.concat("%"));
            list = query.list();
            tx.commit();            
        }catch(HibernateException e){           
            tx.rollback();
            System.out.println("br.com.fametro.dao.CronogramaDao.pesquisarPorNome(): "+e.getMessage());
        }finally {
            if(session.isOpen())
                session.close();
        }
        return list;
    }
}
