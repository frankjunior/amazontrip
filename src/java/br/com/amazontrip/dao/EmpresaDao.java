/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;
import br.com.amazontrip.model.Empresa;
import java.util.List;
import org.hibernate.HibernateException;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;

/**
 *
 * @author n0mercy
 */
public class EmpresaDao extends GenericDao {
    
    Session session;
    Transaction tx;
    List<Empresa> list;
    public Empresa salvar(Empresa empresa) throws HibernateException {
        super.saveOrUpdate(empresa);
        return empresa;
    }
    
    public Empresa buscarPorId(int id) throws HibernateException {
        return (Empresa) super.find(Empresa.class, id);
    }
    
     public List<Empresa> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
     
     public void delete(Empresa empresa) throws HibernateException {
        super.delete(empresa);
    }
     
     public List<Empresa> pesquisarPorNome(String busca){
        session = getSession();
        tx = session.beginTransaction();
        try{
            Query query = session.createQuery("from Empresa e where e.txNome like :param ").setParameter("param", busca.concat("%"));
            list = query.list();
            tx.commit();            
        }catch(HibernateException e){           
            tx.rollback();
            System.out.println("br.com.fametro.dao.EmpresaDao.pesquisarPorNome(): "+e.getMessage());
        }finally {
            if(session.isOpen())
                session.close();
        }
        return list;
    }
}
