/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;

import br.com.amazontrip.model.TipoEmbarcacao;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author n0mercy
 */
public class TipoEmbarcacaoDao extends GenericDao{
    
    public TipoEmbarcacao salvar(TipoEmbarcacao tipoEmbarcacao) throws HibernateException {
        super.saveOrUpdate(tipoEmbarcacao);
        return tipoEmbarcacao;
    }
    
    public TipoEmbarcacao buscarPorId(int id) throws HibernateException {
        return (TipoEmbarcacao) super.find(TipoEmbarcacao.class, id);
    }

    public List<TipoEmbarcacao> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(TipoEmbarcacao tipoEmbarcacao) throws HibernateException {
        super.delete(tipoEmbarcacao);
    }
}
