/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.amazontrip.dao;
import br.com.amazontrip.model.TipoCliente;
import java.util.List;
import org.hibernate.HibernateException;

/**
 *
 * @author n0mercy
 */
public class TipoClienteDao extends GenericDao {

    public TipoCliente salvar(TipoCliente tipoCliente) throws HibernateException {
        super.saveOrUpdate(tipoCliente);
        return tipoCliente;
    }
    
    public TipoCliente buscarPorId(int id) throws HibernateException {
        return (TipoCliente) super.find(TipoCliente.class, id);
    }

    public List<TipoCliente> listar(Class clazz) throws HibernateException {
        return super.findAll(clazz); 
    }
    
    public void delete(TipoCliente tipoCliente) throws HibernateException {
        super.delete(tipoCliente);
    }
}
