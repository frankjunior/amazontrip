package br.com.amazontrip.model;
// Generated Jun 21, 2018 12:23:53 PM by Hibernate Tools 4.3.1


import java.util.HashSet;
import java.util.Set;

/**
 * Empresa generated by hbm2java
 */
public class Empresa  implements java.io.Serializable {


     private Integer id;
     private Usuario usuario;
     private boolean boStatus;
     private String txNome;
     private String txCnpj;
     private String txProprietario;
     private String txTelefone;
     private Set embarcacaos = new HashSet(0);
     private Set funcionarios = new HashSet(0);

    public Empresa() {
    }
   
    public Integer getId() {
        return this.id;
    }
    
    public void setId(Integer id) {
        this.id = id;
    }
    public Usuario getUsuario() {
        return this.usuario;
    }
    
    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }
    public boolean getBoStatus() {
        return this.boStatus;
    }
    
    public void setBoStatus(boolean boStatus) {
        this.boStatus = boStatus;
    }
    public String getTxNome() {
        return this.txNome;
    }
    
    public void setTxNome(String txNome) {
        this.txNome = txNome;
    }
    public String getTxCnpj() {
        return this.txCnpj;
    }
    
    public void setTxCnpj(String txCnpj) {
        this.txCnpj = txCnpj;
    }
    public String getTxProprietario() {
        return this.txProprietario;
    }
    
    public void setTxProprietario(String txProprietario) {
        this.txProprietario = txProprietario;
    }
    public String getTxTelefone() {
        return this.txTelefone;
    }
    
    public void setTxTelefone(String txTelefone) {
        this.txTelefone = txTelefone;
    }
    public Set getEmbarcacaos() {
        return this.embarcacaos;
    }
    
    public void setEmbarcacaos(Set embarcacaos) {
        this.embarcacaos = embarcacaos;
    }
    public Set getFuncionarios() {
        return this.funcionarios;
    }
    
    public void setFuncionarios(Set funcionarios) {
        this.funcionarios = funcionarios;
    }




}


