<!DOCTYPE html>
<html>

    <head>
        <!--Import Google Icon Font-->
        <link href="http://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <!--Import materialize.css-->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/css/materialize.min.css">

        <style type="text/css">
            html,
            body {
                height: 100%;
            }

            html {
                display: table;
                margin: auto;
            }

            body {
                display: table-cell;
                vertical-align: middle;
            }

            .margin {
                margin: 0 !important;
            }
        </style>

        <!--Let browser know website is optimized for mobile-->
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </head>

    <body class="light-green lighten-5">

        <div class="container">
            <div class="row">
                <div class="col s12 m8 offset-m2">
                    <form class="login-form" method="POST" action="ControllerLogin">
                        <div class="card">
                            <div class="card-image">
                                <img src="files/imagens/login-image.jpg" alt=""/>
                                <span class="card-title">
                                    <h2>Login</h2>
                                    <h6>Amazontrip</h6>
                                </span>
                            </div>
                            <div class="card-content">

                                <div class="row">
                                <div class="col s12 m8">
                                    <div class="left-align">
                                        <button onclick="window.location.href='cadastrarCliente.jsp'" type="button" class="btn green yellow-text">Cliente</button>
                                </div>
                                </div>
                                <div class="col s12 m4">
                                    <div class="right-align">
                                    <button type="button" onclick="window.location.href='cadastrarEmpresa.jsp'" class="btn yellow green-text">Empresa</button>
                                </div>
                                </div>
                            </div>
                            </div>
                            <div class="card-action blue-grey lighten-3">
                                <div class="center-align">
                                    <button type="button" onclick="window.location.href='login.jsp'" class="btn blue-grey darken-1">Voltar</button>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>


        <!--Import jQuery before materialize.js-->
        <script type="text/javascript" src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.6/js/materialize.min.js"></script>
    </body>

</html>