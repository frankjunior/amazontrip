<%@page import="br.com.amazontrip.model.Usuario"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link href="../files/dist/jquery.mask.js" type="text/javascript" >   
        <script src="../files/dist/jquery.mask.min.js" type="text/javascript"></script>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="../files/materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
        <link href="../files/materialize/css/estilocss.css" rel="stylesheet" type="text/css"/>
        <link href="../files/materialize/js/materialize.js" type="text/javascript">
        <link href="../files/materialize/js/materialize.min.js" type="text/javascript">
        <link rel="icon" href="../files/imagens/barco_01.jpg" type="image/jpg" sizes="16x16" />

    </head>

    <body>
        <% Usuario user = (Usuario) session.getAttribute("userEntity"); %><!-- tenta recuperar um usuário da sessão -->
        <!-- Cadastrando -->
        <ul id="dropdown1" class="dropdown-content">
            <% if (user == null) {//exibe o menu para usuários não logados %>

            <li><a href="../cadastrarCliente.jsp">Cliente</a></li>
            <li><a href="../paginas/cadastrarFuncionario.jsp">Funcionário</a></li>
            <li><a href="../cadastrarEmpresa.jsp">Empresa</a></li>
            <li><a href="../paginas/cadastrarEmbarcacao.jsp">Embarcação</a></li>
            <li><a href="../paginas/cadastrarCronograma.jsp">Cronograma</a></li>
            <li><a href="../paginas/cadastrarEscala.jsp">Escalas</a></li>

            <%} else {//exibe o menu para usuários logados%>
            <% if (user.getTipoUsuario().getId() == 2 || user.getTipoUsuario().getId() == 1) { //permissão de usuário do tipo Admin e Empresa%>
            <li><a href="../cadastrarCliente.jsp">Cliente</a></li>
            <li><a href="../paginas/cadastrarFuncionario.jsp">Funcionário</a></li>
            <li><a href="../paginas/cadastrarEmbarcacao.jsp">Embarcação</a></li>
            <li><a href="../paginas/cadastrarCronograma.jsp">Cronograma</a></li>
            <li><a href="../paginas/cadastrarEscala.jsp">Escalas</a></li>
                <%}%>
                <% if (user.getTipoUsuario().getId() == 1) {//permissão de usuário do tipo Admin e Empresa%>
            <li><a href="../cadastrarEmpresa.jsp">Empresa</a></li>
                <%}%> 

            <%}%>
        </ul>
        <!-- Pesquisando -->

        <ul id="dropdown2" class="dropdown-content">
            <% if (user == null) { %><!-- usuário não logado -->
            <li><a href="../paginas/listandoCliente.jsp">Cliente</a></li>
            <li><a href="../paginas/listandoFuncionario.jsp">Funcionário</a></li>
            <li><a href="../paginas/listandoEmpresa.jsp">Empresa</a></li>
            <li><a href="../paginas/listandoEmbarcacao.jsp">Embarcação</a></li>
            <li><a href="../paginas/listandoCronograma.jsp">Cronograma</a></li>
            <li><a href="../paginas/listandoEscala.jsp">Escalas</a></li>

            <%} else {%><!-- usuário logado -->
            <% if (user.getTipoUsuario().getId() == 2 || user.getTipoUsuario().getId() == 1) { %><!-- tipo Admin e Empresa podem visualizar as opções -->
            <li><a href="../paginas/listandoCliente.jsp">Cliente</a></li>
            <li><a href="../paginas/listandoFuncionario.jsp">Funcionário</a></li>
            <li><a href="../paginas/listandoEmpresa.jsp">Empresa</a></li>
            <li><a href="../paginas/listandoEmbarcacao.jsp">Embarcação</a></li>
            <li><a href="../paginas/listandoCronograma.jsp">Cronograma</a></li>
            <li><a href="../paginas/listandoEscala.jsp">Escalas</a></li>
                <%}
                    }%>
        </ul> 
        <nav>
            <div class="nav-wrapper green">

                <%
                    if (session.getAttribute("userEntity") != null) {//caso haja sessão aberta, habilita o botão Sair
                %>
                <span class="right">${user.txLogin} : <%= user.getTipoUsuario().getTxNome()%> <a class="btn-flat white-text" href="../ControllerLogin?action=logoff">Sair</a> </span>
                <%
                    }
                %>
                <ul class="left hide-on-med-and-down">
                    <li><a href="../home.jsp">Início</a></li>
                    <!-- Dropdown Trigger -->
                    <li><a id="dropdownCadastro" class="dropdown-button" data-activates="dropdown1" data-hover="true">Cadastro</a></li>
                    <li><a id="dropdownPesquisa" class="dropdown-button" data-activates="dropdown2" data-hover="true">Pesquisa</a></li>
                        <% if (user != null) {//compra de passagem para os tipos Admin, Funcionario e Cliente
                                if (user.getTipoUsuario().getId() == 4 || user.getTipoUsuario().getId() == 1 || user.getTipoUsuario().getId() == 3) { %>
                    <li><a href="../paginas/compraPassagem.jsp">Compre aqui</a></li>
                        <%}
                        %>
                        <% if (user != null && user.getTipoUsuario().getId() == 1) { %><!-- somente Admin com acesso -->
                    <li><a href="../paginas/gerarRelatorio.jsp">Relatórios</a></li>
                        <%}
                            }%>
                    <li><a href="quemSomos.jsp">Quem somos?</a></li>                    

                </ul>
            </div>
        </nav>

    </body>
</html>
