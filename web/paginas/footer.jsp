<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link href="../files/materialize/css/estilocss.css" rel="stylesheet" type="text/css"/>
        <link href="../files/materialize/css/materialize.css" rel="stylesheet" type="text/css"/>
    </head>
    <body>
        <br/><br/><br/><br/><br/><br/><br/><br/>
        <footer class="page-footer green" style="position:fixed;bottom:0;left:0;width:100%;height: 20%;" id="rodape">
            <div class="container">
                <div class="row">
                    <div class="col l6 s12">
                        <h5 class="white-text">Amazon Trip</h5>
                        <p class="grey-text text-lighten-4">Sua melhor viagem é aqui.</p>
                    </div>
                    <div class="col l4 offset-l2 s12">
                        <h5 class="white-text">Links</h5>
                        <ul>
                            <li><a class="grey-text text-lighten-3" href="#!">Fale Conosco</a></li>
                            <li><a class="grey-text text-lighten-3" href="#!">Trabalhe Conosco</a></li>
                            <li><a class="grey-text text-lighten-3" href="#!">Sobre AmazonTrip</a></li>
                            <!--   <li><a class="grey-text text-lighten-3" href="#!"></a></li> -->
                        </ul>
                    </div>
                </div>
            </div>
            <div class="footer-copyright">
                <div class="container">
                    Copyright ©2018 amazontrip.com.br LTDA
                    <a class="grey-text text-lighten-4 right" href="#!">Mais Links</a>
                </div>
            </div>
        </footer>

        <script src="../files/materialize/js/materialize.min.js" type="text/javascript"></script> 
        <!-- Compiled and minified JavaScript -->
        <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0-beta/js/materialize.min.js"></script>
        <script src="../files/materialize/js/jquery-3.2.1.min.js" type="text/javascript"></script>
        <script src="../files/materialize/js/materialize.js" type="text/javascript"></script>
        <script src="../files/materialize/js/funcoes.js" type="text/javascript"></script>
        <script src="../files/dist/jquery.mask.min.js" type="text/javascript"></script>
        <script src="../files/dist/jquery.mask.js" type="text/javascript"></script>
        <script type="text/javascript">
            $(document).ready(function () {
                $('.date').mask('00/00/0000');
                $('.time').mask('00:00');
                $('.date_time').mask('00/00/0000 00:00');
                $('.cep').mask('00000-000');
                $('.phone').mask('0000-0000');
                $('.phone_with_ddd').mask('(00)00000-0000');
                $('.phone_us').mask('(000) 000-0000');
                $('.mixed').mask('AAA 000-S0S');
                $('.cpf').mask('000.000.000-00', {reverse: true});
                $('.cnpj').mask('00.000.000/0000-00', {reverse: true});
                $('.rg').mask('000000-0', {reverse: true});
                $('.money').mask('000.000.000.000.000,00', {reverse: true});
            });
        </script>
        <script>
            $(document).ready(function () {
                $('.modal').modal();
                alert(d);
            });
        </script>
        <script>
            function showCronograma(val) {
                var x = document.getElementById(val).value;
                alert(x);
            }
        </script>
    </body>    
</html>
