
<%@page import="br.com.amazontrip.model.Empresa"%>
<%@page import="br.com.amazontrip.model.TipoEmbarcacao"%>
<%@page import="br.com.amazontrip.dao.EmbarcacaoDao"%>
<%@page import="br.com.amazontrip.model.Embarcacao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../files/materialize/css/estilocss.css" rel="stylesheet" type="text/css"/>
        <title>:: Embarcação ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>
        <div class="container">

            <h2> Listando Embarcação </h2>

            <%
                List<Embarcacao> listEmbarcacao = new ArrayList<Embarcacao>();
                List<TipoEmbarcacao> tipoEmbarcacao = new ArrayList<TipoEmbarcacao>();
                List<Empresa> listEmpresa = new ArrayList<Empresa>();
                if (request.getParameter("busca") != null) {
                    if (request.getParameter("busca").trim().isEmpty()) {
                        listEmbarcacao = new EmbarcacaoDao().listar(Embarcacao.class);
                    } else {
                        listEmbarcacao = new EmbarcacaoDao().pesquisarPorNome(request.getParameter("busca"));
                    }
                }
            %>
            <form action="" method="post">
                <table>
                    <tr>
                        <td width="60%"> 
                            <div class="input-field col s6">
                                <input id="busca" name="busca" type="text"  placeholder="Pesquisa" class="validate">
                            </div>
                        </td>
                        <td width="20%">
                            <button type="submit" name="btn_buscar" class="btn" id="btn_alterar">Pesquisar</button>
                        </td>
                        <td width="20%">
                            <input type="button" onclick="window.location.href = 'cadastrarEmbarcacao.jsp'" name="btn_buscar" class="btn" value="Novo" id="btn_alterar"/>
                        </td>
                    </tr>
                </table>
                        <% if(listEmbarcacao.size() > 0){ %>
                    <table class="striped" id="listaClientes">
                        <thead>
                            <tr>
                                <th> CÓDIGO </th>
                                <th> NOME </th>
                                <th> CONTATO </th>

                            </tr>
                        <thead>
                            <%
                                for (Embarcacao embarcacao : listEmbarcacao) {
                            %>
                        <tbody>
                            <tr>
                                <td> <%= embarcacao.getId()%> </td> 
                                <td width="35%"> <%= embarcacao.getTxNome()%> </td>
                                <td> <%= embarcacao.getTxContato() %> </td>


                                <td >
                                    <input type="button" onclick="window.location.href='../RedirectServlet?id_embarcacao=<%=embarcacao.getId()%>&pagina=listandoEmbarcacao&action=alterar'" name="btn_alterar" value="Alterar" class="btn blue-grey" id="btn_alterar"/>
                                </td> 

                                <td>
                                <% if (embarcacao.getBoStatus()) {%>
                                <button type="button" onclick="window.location.href = '../ControllerEmbarcacao?page=listandoEmbarcacao&Action=desativar&Id=<%=embarcacao.getId()%>'" class="btn orange" id="btnExcluir" name="btnExcluir">Desativar</button>
                                <%} else {%>
                                <button type="button" onclick="window.location.href = '../ControllerEmbarcacao?page=listandoEmbarcacao&Action=ativar&Id=<%=embarcacao.getId()%>'" class="btn green" id="btnExcluir" name="btnExcluir">Ativar</button>
                                <%}%>
                            </td>
                            </tr>  
                        </tbody>
                        <%
                            }
                        %>
                    </table> 
                    <% } %>
                <br/>
            </form> 
        </div>
    </body>

    <%@include file="footer.jsp" %>
</html>

