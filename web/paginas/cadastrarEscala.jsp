<%@page import="br.com.amazontrip.dao.CronogramaDao"%>
<%@page import="br.com.amazontrip.model.Cronograma"%>
<%@page import="br.com.amazontrip.dao.CidadeDao"%>
<%@page import="br.com.amazontrip.dao.EmbarcacaoDao"%>
<%@page import="br.com.amazontrip.util.SemanaEnum"%>
<%@page import="br.com.amazontrip.model.Embarcacao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="br.com.amazontrip.model.Cidade"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Escala ::</title>
    </head>

    <%@include file="top.jsp"%>

    <body>
        <%
            List<Cidade> cidades = new ArrayList<Cidade>();
            List<Cronograma> cronogramas = new ArrayList<Cronograma>();
            cidades = new CidadeDao().listar(Cidade.class);
            cronogramas = new CronogramaDao().listar(Cronograma.class);
            List<Embarcacao> embarcacoes = new ArrayList<Embarcacao>();
            embarcacoes = new EmbarcacaoDao().listar(Embarcacao.class);
            SemanaEnum dias[] = SemanaEnum.values();
        %>
        <div class="container">
            <form class="col s6" name="cadastrarEscala" id="cadastrarEscala" action="../ControllerEscala" method="post">
                <h4>Adicionar Escalas ao Cronograma</h4>
                <div class="row">

                    <div class="input-field col s6">
                        <select required id="cronograma" name="cronograma">
                            <option value="" disabled selected>Selecione</option>
                            <%
                                for (Cronograma cronograma : cronogramas) {
                            %>
                            <option value="<%=cronograma.getId()%>"><% out.print(cronograma.getTxDescricao()); %></option>
                            <%
                                }
                            %>
                        </select>
                        <label>Cronograma</label>
                    </div>
                    <div class="input-field col s6">
                        <select required name="escala">
                            <option value="" disabled selected>Selecione</option>
                            <%
                                for (Cidade cidade : cidades) {
                            %>
                            <option value="<%=cidade.getId()%>"><% out.print(cidade.getTxNome()); %></option>
                            <%
                                }
                            %>
                        </select>
                        <label>Escala</label>
                    </div>
                </div>
                <div class="row">                  
                    <div class="input-field col s6">
                        <select required name="dia_semana">
                            <option value="" disabled select>Dia da semana</option>
                            <%
                                for (SemanaEnum diaSaida : dias) {
                            %>
                            <option value="<%=diaSaida.getValor()%>"><% out.print(diaSaida); %></option>>
                            <%
                                }
                            %>
                        </select>
                        <label>Selecione o dia de saída</label>
                    </div>
                    <div class="input-field col s2">
                        <div>
                            <input id="hora_saida" name="data_hora" required class="time" type="text">
                            <label for="hora_saida">Horario</label>
                        </div>
                    </div>

                    <div class="input-field col s2">
                        <div>
                            <input id="valor_padrao" name="valor_padrao" required class="money" type="text">
                            <label for="valor_padrao">Valor Padrão</label>
                        </div>
                    </div>
                    <div class="input-field col s2">
                        <div>
                            <input id="valor_camarote" name="valor_camarote" required class="money" type="text">
                            <label for="valor_camarote">Valor Camarote</label>
                        </div>
                    </div>

                </div> 
                <br/>

                <div class="row col s4">
                    <div class="col s2">
                        <input type="hidden" name="page" value="cadastrarEscala"/>
                        <button type="submit" value="Cadastrar" class="btn" id="btnCadastrar">Cadastrar</button>
                    </div>
                    <div class="col s2">
                        <button type="reset" class="btn orange" value="Cancelar" id="btnCancelar">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>
    </body>
    <%@include file="footer.jsp" %>
</html>
