<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.amazontrip.model.Cronograma"%>
<%@page import="br.com.amazontrip.dao.CidadeDao"%>
<%@page import="br.com.amazontrip.dao.EmbarcacaoDao"%>
<%@page import="br.com.amazontrip.util.SemanaEnum"%>
<%@page import="br.com.amazontrip.model.Embarcacao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="br.com.amazontrip.model.Cidade"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Cronogramas ::</title>
    </head>

    <%@include file="top.jsp"%>

    <body>
        <%
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            Cronograma cronograma = new Cronograma();
            List<Cidade> cidades = new ArrayList<Cidade>();
            cidades = new CidadeDao().listar(Cidade.class);
            List<Embarcacao> embarcacoes = new ArrayList<Embarcacao>();
            embarcacoes = new EmbarcacaoDao().listar(Embarcacao.class);
            SemanaEnum dias[] = SemanaEnum.values();
            if (session.getAttribute("entity") != null) {
                cronograma = (Cronograma) session.getAttribute("entity");//recupera o cronograma vindo do controller RedirectServlet
        %>
        <div class="container">
            <form class="col s6" name="cadastrarCronograma" id="cadastrarCronograma" action="../ControllerCronograma" method="post">
                <h4>Alteração de Cronograma {ID: <%=cronograma.getId()%>}</h4>
                <div class="row">

                    <div class="input-field col s6">
                        <select required name="embarcacao">
                            <option value="" disabled selected>Escolha a embarcação</option>
                            <%
                                for (Embarcacao embarcacao : embarcacoes) {
                            %>
                            <%
                                if (embarcacao.getId() == cronograma.getEmbarcacao().getId()) {
                            %>
                            <option value="<%=embarcacao.getId()%>" selected><% out.print(embarcacao.getTxNome()); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%=embarcacao.getId()%>"><% out.print(embarcacao.getTxNome()); %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Selecione a embarcação</label>
                    </div>
                    <div class="input-field col s6">
                        <div>
                            <input id="local_partida" name="local_partida" value="<%= cronograma.getTxLocalPartida() %>" required type="text">
                            <label for="local_partida">Local de Partida</label>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s6">
                        <select required name="cidade_origem">
                            <option value="" disabled selected>Escolha a origem</option>
                            <%
                                for (Cidade cidadeOrigem : cidades) {
                                    if (cidadeOrigem.getId() == cronograma.getCidadeByIdCidadeOrigem().getId()) {
                            %>
                            <option value="<%=cidadeOrigem.getId()%>" selected><% out.print(cidadeOrigem.getTxNome()); %></option>
                            <%
                            } else {
                            %>                            
                            <option value="<%=cidadeOrigem.getId()%>"><% out.print(cidadeOrigem.getTxNome()); %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Selecione a origem</label>
                    </div>

                    <div class="input-field col s6">
                        <select required name="cidade_destino">
                            <option value="" disabled selected>Escolha o destino</option>
                            <%
                                for (Cidade cidadeDestino : cidades) {
                                    if (cidadeDestino.getId() == cronograma.getCidadeByIdCidadeDestino().getId()) {
                            %>
                            <option value="<%=cidadeDestino.getId()%>" selected><% out.print(cidadeDestino.getTxNome()); %></option>
                            <%
                            } else {
                            %>                            
                            <option value="<%=cidadeDestino.getId()%>"><% out.print(cidadeDestino.getTxNome()); %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Selecione o destino</label>
                    </div>

                    <div class="input-field col s6">
                        <select required name="dia_saida">
                            <option value="" disabled select>Dia da semana</option>
                            <%
                                for (SemanaEnum diaSaida : dias) {
                                    if (Integer.parseInt(String.valueOf(cronograma.getNbDiaSemanaSaida())) == diaSaida.getValor()) {
                            %>
                            <option value="<%=diaSaida.getValor()%>" selected><% out.print(diaSaida); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%=diaSaida.getValor()%>"><% out.print(diaSaida); %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Selecione o dia de saída</label>
                    </div>

                    <div class="input-field col s6">
                        <select required name="dia_chegada">
                            <option value="" disabled select>Dia da semana</option>
                            <%
                                for (SemanaEnum diaChegada : dias) {
                                    if (Integer.parseInt(String.valueOf(cronograma.getNbDiaSemanaChegada())) == diaChegada.getValor()) {
                            %>
                            <option value="<%=diaChegada.getValor()%>" selected><% out.print(diaChegada); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%=diaChegada.getValor()%>"><% out.print(diaChegada); %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Selecione o dia de chegada</label>
                    </div>
                    <div class="input-field col s6">
                        <div>
                            <input id="hora_saida" name="hora_saida" value="<%=sdf.format(cronograma.getNbHorarioSaida()) %>" required class="time" type="text">
                            <label for="hora_saida">Data/Hora Partida</label>
                        </div>
                    </div>

                    <div class="input-field col s6">
                        <div>
                            <input id="hora_chegada" name="hora_chegada" value="<%=sdf.format(cronograma.getNbHorarioChegada()) %>" required class="time" type="text">
                            <label for="hora_chegada">Data/Hora Chegada</label>
                        </div>
                    </div>



                </div> 
                <br/>

                <div class="row col s4">
                    <div class="col s2">
                        <input type="hidden" name="page" value="alterarCronograma"/>
                        <input type="hidden" name="id_cronograma" value="<%=cronograma.getId() %>"/>
                        <button type="submit" value="Cadastrar" class="btn" id="btnCadastrar">Alterar</button>
                    </div>
                    <div class="col s2">
                        <button type="button" class="btn" value="Cancelar" onclick="window.location.href='listandoCronograma.jsp'" id="btnCancelar">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>      
    </body>
    <%@include file="footer.jsp" %>
</html>
