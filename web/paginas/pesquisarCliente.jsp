<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Pesquisar Cliente ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>
        <div class="container">
        <div >
            <h4> <i></i><span>pesquisa de Clientes </span> </h4>
        </div>

        <div >
            <div class="row" >
                <div >
                    <table >
                        <tr>
                            <td >
                                <button class="btn" onclick="location.href = 'cadastrarCliente.jsp'">Novo Cliente</button>
                            </td>
                            <td >
                                <input class="validate" name="nome" id="txtPesquisa" type="text" required placeholder="Digite o nome do Cliente"/>
                            </td>
                            <td >
                                <button class="btn" onclick="location.href = 'clientelistar.jsp?txtPesquisa=' + $('#txtPesquisa').val() + '';" ></button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
           
            <div class="row" >
            <table style="width:100%;margin-bottom:0; ">
                <thead>
                    <tr>
                        <th > Codigo</th>
                        <th > Login </th>
                        <th > Senha </th>
                        <th > Nome </th>
                        <th > RG </th>
                        <th > CPF </th>
                        <th > Telefone </th>
                        <th > Data de Nasc. </th>                                            
                    </tr>
                </thead>
                <tbody>
                    <%--  <%
                      for (Cliente c : listCliente) 
                      {
                      %> --%>

                    <tr>
                        <td > <%-- <%=c.getClicod()%> --%> </td>
                        <td > <%-- <%=c.getClinome()%> --%> </td>
                        <td > <%-- <%=c.getClicpf()%> --%> </td>
                        <td > <%-- <%=c.getClirg()%> --%> </td>
                        <td > <%-- <%=c.getCliemail()%> --%> </td>
                        <td > <%-- <%=c.getClifone01()%> --%> </td>
                        <td >
                            <div > 
                                <a onclick="location.href = 'clienteAlterar.jsp?codigo= <%-- <%=c.getClicod()%> --%>';">
                                    <i></i>
                                </a> 
                                <a>
                                    <i></i>
                                </a> 
                            </div>
                        </td>
                    </tr>

                    <%--  <%
                          }
                      %> --%>
                </tbody>
            </table>
              
             </div>

            <div >

                <!--div class="span6">
                  <div class="pagination pull-left ">
                    <ul>
                      <li><a href="#">Anterior</a></li>
                      <li><a href="#">1</a></li>
                      <li><a href="#">2</a></li>
                      <li><a href="#">3</a></li>
                      <li><a href="#">4</a></li>
                      <li><a href="#">Proximo</a></li>
                    </ul>
                  </div >
                </div-->
            </div>
        </div>
        </div>

    </body>

    <%@include file="footer.jsp" %>
</html>

