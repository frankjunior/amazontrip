
<%@page import="br.com.amazontrip.dao.TipoEmbarcacaoDao"%>
<%@page import="br.com.amazontrip.dao.EmpresaDao"%>
<%@page import="br.com.amazontrip.model.Embarcacao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="br.com.amazontrip.model.Empresa"%>
<%@page import="br.com.amazontrip.model.TipoEmbarcacao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Embarcações ::</title>
    </head>

    <%@include file="top.jsp"%>

    <body>

        <div class="container">
            <%
                List<TipoEmbarcacao> tipoEmbarcacao;
                List<Empresa> listEmpresa;
                Embarcacao embarcacao = new Embarcacao();
                if (session.getAttribute("entity") != null) {
                    embarcacao = (Embarcacao) session.getAttribute("entity");//recupera a embarcação vindo do controller RedirectServlet
                }
            %>
            <form class="col s12" name="cadastrarEmbarcacao" id="cadastrarEmbarcacao" action="../ControllerEmbarcacao" method="post">
                <div class="row">
                    <h4>Alterar Embarcação {ID: <%=embarcacao.getId()%>}</h4>
                </div> 

                <div class="row">
                    <!-- Inscrição da Capitania -->
                    <div class="input-field col s6">
                        <input name="inscricao" id="inscricao" type="text" readonly="" required value="<%=embarcacao.getNbInscricaoCapitania()%>" class="validate">
                        <label for="inscricao">Inscrição da Capitania</label>
                    </div>   

                    <!-- Nome -->
                    <div class="input-field col s6">
                        <input name="nomeEmbarcacao" id="nomeEmbarcacao" value="<%=embarcacao.getTxNome()%>" type="text" required class="validate">
                        <label for="nomeEmbarcacao">Nome da Embarcação</label>
                    </div>          
                </div>


                <div class="row">
                    <!-- Tipo da Embarcação -->
                    <div class="input-field col s6">                         
                        <select id="tipoEmbarcacao" name="tipoEmbarcacao" required>
                            <option selected disabled>Selecione</option>
                            <%
                                tipoEmbarcacao = new ArrayList<TipoEmbarcacao>();
                                tipoEmbarcacao = new TipoEmbarcacaoDao().listar(TipoEmbarcacao.class);
                                for (TipoEmbarcacao tipo : tipoEmbarcacao) {
                            %>
                            <%
                                if (embarcacao.getTipoEmbarcacao().getId() == tipo.getId()) {
                            %>
                            <option value="<%=tipo.getId()%>" selected><% out.print(tipo.getTxNome()); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%=tipo.getId()%>"><% out.print(tipo.getTxNome()); %></option>
                            <%
                                }
                            %>
                            <%
                                }
                            %>
                        </select>
                        <label>Selecione o tipo da embarcação</label>
                    </div>

                    <div class="input-field col s6">   
                        <!-- Empresa Dona da Embarcação -->
                        <select id="empresa" name="empresa" required>
                            <option selected disabled>Selecione</option>
                            <%
                                listEmpresa = new ArrayList<Empresa>();
                                listEmpresa = new EmpresaDao().listar(Empresa.class);
                                for (Empresa empresa : listEmpresa) {
                            %>
                            <%
                                if (embarcacao.getEmpresa().getId() == empresa.getId()) {
                            %>
                            <option value="<%=empresa.getId()%>" selected><% out.print(empresa.getTxNome()); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%=empresa.getId()%>"><% out.print(empresa.getTxNome()); %></option>
                            <%
                                }
                            %>
                            <%
                                }
                            %>
                        </select>
                        <label>Selecione a empresa</label>
                    </div>                    
                </div>

                <div class="row">
                    <!-- Capacidade da Embarcação -->
                    <div class="input-field col s6">
                        <input name="capacidade" id="capacidade" value="<%=embarcacao.getNbCapacidade()%>" required type="number" class="validate">
                        <label for="capacidade">Capacidade</label> 
                    </div>
                </div>

                <div class="row">
                    <!-- TV -->
                    <div class="col s6">
                        <p><h7>A embarcação possui TV?</h7></p>
                        <p>
                            <%
                            if(embarcacao.isBoTv()){
                            %>
                            <input class="with-gap" name="tv" type="radio" id="possuiTV" checked value="true"  />
                            <label for="possuiTV">Sim</label>
                            <input class="with-gap" name="tv" type="radio" id="naoPossuiTV" value="false" />
                            <label for="naoPossuiTV">Não</label>
                            <%
                            }else{
                            %>
                            <input class="with-gap" name="tv" type="radio" id="possuiTV"  value="true"  />
                            <label for="possuiTV">Sim</label>
                            <input class="with-gap" name="tv" type="radio" id="naoPossuiTV" checked value="false" />
                            <label for="naoPossuiTV">Não</label>
                            <%
                            }
                            %>
                        </p>
                    </div>

                    <!-- Área de Lazer -->
                    <div class="col s6">
                        <p><h7>A embarcação possui área de lazer?</h7></p>
                        <p>
                            <% if (embarcacao.isBoAreaLazer()) { %>
                            <input class="with-gap" name="areaLazer" type="radio" checked id="possuiLazer" value="true" />
                            <label for="possuiLazer">Sim</label>
                            <input class="with-gap" name="areaLazer" type="radio"id="naoPossuiLazer" value="false"/>
                            <label for="naoPossuiLazer">Não</label>
                            <%
                            } else {
                            %>
                            <input class="with-gap" name="areaLazer" type="radio" id="possuiLazer" value="true" />
                            <label for="possuiLazer">Sim</label>
                            <input class="with-gap" name="areaLazer" type="radio" checked id="naoPossuiLazer" value="false"/>
                            <label for="naoPossuiLazer">Não</label>
                            <%
                                }
                            %>
                        </p>
                    </div>
                </div>       

                <!-- Climatização -->
                <div class="row">
                    <div class="col s6">
                        <p><h7>A embarcação possui climatização?</h7></p>

                        <p> 
                            <%
                            if(embarcacao.isBoClimatizacao()){
                            %>
                            <input class="with-gap" name="climatizacao" type="radio" checked id="possuiClimatizacao" value="true" />
                            <label for="possuiClimatizacao">Sim</label>
                            <input class="with-gap" name="climatizacao" type="radio" id="naoPossuiClimatizacao" value="false" />
                            <label for="naoPossuiClimatizacao">Não</label>
                            <%
                            }else{
                            %>
                            <input class="with-gap" name="climatizacao" type="radio"  id="possuiClimatizacao" value="true" />
                            <label for="possuiClimatizacao">Sim</label>
                            <input class="with-gap" name="climatizacao" type="radio" checked id="naoPossuiClimatizacao" value="false" />
                            <label for="naoPossuiClimatizacao">Não</label>
                            <%
                            }
                            %>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s6">
                        <input name="camarote" id="camarote" value="<%=embarcacao.getNbQuantidadeCamarote()%>" required type="number" class="validate">
                        <br> <label for="camarote"> Quantidade de camarotes</label> 
                    </div>

                    <div class="input-field col s6">
                        <input name="contato" id="contato" required value="<%=embarcacao.getTxContato()%>" type="text" class="phone_with_ddd">
                        <br> <label for="contato">Contato</label> 
                    </div>
                </div>

                <div class="row col s4">
                    <div class="col s2">
                        <input type="hidden" name="page" value="alterarEmbarcacao"/>
                        <input type="hidden" name="id_embarcacao" value="<%=embarcacao.getId()%>"/>
                        <button type="submit" value="Salvar" class="btn" id="btnCadastrar">Salvar</button>
                    </div>
                    <div class="col s2">
                        <button type="button" onclick="window.location.href='listandoEmbarcacao.jsp'" class="btn orange" value="Cancelar" id="btnCancelar">Cancelar</button>
                    </div>
                </div>
            </form>

        </div>

    </body>
    <%@include file="footer.jsp" %>
</html>
