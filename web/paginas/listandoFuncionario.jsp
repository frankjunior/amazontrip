
<%@page import="br.com.amazontrip.dao.FuncionarioDao"%>
<%@page import="br.com.amazontrip.model.Funcionario"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../files/materialize/css/estilocss.css" rel="stylesheet" type="text/css"/>
        <title>:: Funcionário ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>
        <div class="container">

            <h2> Listando Funcionários </h2>

            <%
                List<Funcionario> list = new ArrayList<Funcionario>();
                if (request.getParameter("busca") != null) {
                    if (request.getParameter("busca").trim().isEmpty()) {
                        list = new FuncionarioDao().listar(Funcionario.class);
                    } else {
                        list = new FuncionarioDao().pesquisarPorNome(request.getParameter("busca"));
                    }
                }
            %>
            <form action="" method="post">
                <table>
                    <tr>
                        <td width="60%"> 
                            <div class="input-field col s6">
                                <input id="busca" name="busca" type="text"  placeholder="Pesquisa" class="validate">
                            </div>
                        </td>
                        <td width="20%">
                            <button type="submit" name="btn_buscar" onclick="showTable('resultList');" class="btn" id="btn_alterar">Pesquisar</button>
                            <input type="hidden" id="resultList" value="<%= list.size()%>"/>
                        </td>
                        <td width="20%">
                            <input type="button" onclick="window.location.href = 'cadastrarFuncionario.jsp'" name="btn_buscar" class="btn" value="Novo" id="btn_alterar"/>
                        </td>
                    </tr>
                </table>
                <% if (list.size() > 0) { %>
                <table class="striped" id="listaClientes">
                    <thead>
                        <tr>
                            <th> CÓDIGO </th>
                            <th> NOME </th>
                            <th> CPF </th>

                        </tr>
                    <thead>
                        <%
                            for (Funcionario funcionario : list) {
                        %>
                    <tbody>
                        <tr>
                            <td> <%= funcionario.getId()%> </td> 
                            <td width="35%"> <%= funcionario.getTxNome()%> </td>
                            <td> <%= funcionario.getTxCpf()%> </td>


                            <td >
                                <input type="button" onclick="window.location.href = '../RedirectServlet?id_funcionario=<%=funcionario.getId()%>&pagina=listandoFuncionario&action=alterar'" name="btn_alterar" value="Alterar" class="btn blue-grey" id="btn_alterar"/>
                            </td> 

                            <td>
                                <% if (funcionario.getBoStatus()) {%>
                                <button type="button" onclick="window.location.href = '../ControllerFuncionario?page=listandoFuncionario&Action=desativar&Id=<%=funcionario.getId()%>'" class="btn orange" id="btnExcluir" name="btnExcluir">Desativar</button>
                                <%} else {%>
                                <button type="button" onclick="window.location.href = '../ControllerFuncionario?page=listandoFuncionario&Action=ativar&Id=<%=funcionario.getId()%>'" class="btn green" id="btnExcluir" name="btnExcluir">Ativar</button>
                                <%}%>
                            </td>
                        </tr>  
                    </tbody>
                    <%
                        }
                    %>
                </table> 
                <% }%>
                <br/>
            </form> 
        </div>
    </body>

    <%@include file="footer.jsp" %>
</html>

