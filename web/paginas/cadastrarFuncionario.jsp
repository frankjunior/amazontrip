

<%@page import="br.com.amazontrip.dao.EmpresaDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="br.com.amazontrip.model.Empresa"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset=UTF-8">
        <title>:: Cadastro de Funcionário ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>

        <div class="container">


            <h4>Cadastro de Funcionário</h4>
            <%
            List<Empresa> listEmpresa = new ArrayList<Empresa>();
            listEmpresa = new EmpresaDao().listar(Empresa.class);
            %>
            <form class="col s12" name="cadastrarFuncionario" id="cadastrarFuncionario" action="../ControllerFuncionario" method="post">

                <div class="row">
                    <!-- CPF -->
                    <div class="input-field col s4">
                        <input id="cpf" name="cpf" type="text" class="validate">
                        <label for="cpf">CPF</label>
                    </div>
                    <!-- Nome -->
                    <div class="input-field col s4">
                        <input id="nome" name="nome" type="text" class="validate">
                        <label for="nome">Nome</label>
                    </div>    
                    <!-- Senha -->

                </div>

                <div class="row"> 
                    <div class="input-field col s4">
                        <input id="login" name="login" type="text" class="validate" maxlength="20">
                        <label for="login"> Login </label>
                    </div>
                    <div class="input-field col s4">
                        <input id="senha" name="senha" type="password" class="validate">
                        <label for="senha">Senha</label>
                    </div>
                </div>
                <div class="row">
                    <div class="input-field col s4">                        
                        <select id="empresa" name="empresa">
                            <option value="" disabled selected>Selecione</option>
                            <%
                            for(Empresa empresa: listEmpresa){
                            %>
                            
                            <option value="<%= empresa.getId() %>"><% out.print(empresa.getTxNome()); %></option>
                            <%
                            }
                            %>
                        </select>
                        <label>Selecione a empresa:</label>
                    </div> 

                </div>

                <script>
                    function msgCadFuncionario()
                    {
                        alert("Funcionário cadastrado com sucesso!");
                    }
                </script>

                <br/>
                <div class="row col s4">
                    <div class="col s2">
                        <button type="submit" name="cadastrar" value="cadastrar" class="btn green" id="btnCadastrar">Cadastrar</button>
                    </div>
                    <div class="col s2">
                        <button type="reset" onclick="return confirm('Deseja cancelar o cadastro do Funcionário?');" class="btn red darken-2" value="cancelar" id="btnCancelar">Cancelar</button>
                    </div>
                </div>
                <input type="hidden" name="page" value="cadastrarFuncionario"/>
            </form>          

        </div>
    </body>

    <br />
    <br />
    <br />

    <%@include file="footer.jsp" %>

</html>
