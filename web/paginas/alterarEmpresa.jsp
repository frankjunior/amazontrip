<%@page import="br.com.amazontrip.model.Empresa"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Empresa ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>
        <div class="container">
            <%
                Empresa empresa = new Empresa();
                if (session.getAttribute("entity") != null) {
                    empresa = (Empresa) session.getAttribute("entity");//recupera a empresa vindo do controller RedirectServlet
                }
            %>
            <h4>Alteração de Empresa {ID: <%= empresa.getId()%>}</h4>

            <form class="col s12" name="cadastrarEmpesa" id="cadastrarEmpresa" action="../ControllerEmpresa" method="post">

                <div class="row">
                    <!-- Nome -->
                    <div class="input-field col s6">
                        <input id="nome" name="nome" type="text" maxlength="50" value="<%= empresa.getTxNome()%>" class="validate">
                        <label for="nome">Nome</label>
                    </div>

                    <!-- CNPJ -->
                    <div class="input-field col s6">
                        <input id="cnpj" name="cnpj" type="text" maxlength="18" value="<%= empresa.getTxCnpj()%>" class="cnpj">
                        <label for="cnpj">CNPJ</label>
                    </div>
                </div>

                <div class="row">
                    <!-- Proprietário -->
                    <div class="input-field col s6">
                        <input id="proprietario" name="proprietario" type="text" value="<%= empresa.getTxProprietario()%>" maxlength="50" class="validate">
                        <label for="proprietario">Proprietário</label>
                    </div>   

                    <!-- Telefone -->
                    <div class="input-field col s6">
                        <input id="telefone" name="telefone" type="tel" maxlength="17" value="<%= empresa.getTxTelefone()%>" class="phone_with_ddd">
                        <label for="telefone">Telefone</label>
                    </div> 
                </div> 

                <div class="row"> 
                    <div class="input-field col s6">
                        <input id="login" name="login" type="text" class="validate" value="<%= empresa.getUsuario().getTxLogin()%>" maxlength="20">
                        <label for="login"> Login </label>
                    </div>
                    <div class="input-field col s6">
                        <input id="senha" name="senha" type="password" value="<%= empresa.getUsuario().getTxSenha()%>" class="validate">
                        <label for="senha">Senha</label>
                        <img width="20" height="20" src="files/imagens/showPass.png" onclick="showPassword()" title="exibir senha"/>
                    </div>
                </div>

                <div class="row col s4">
                    <div class="col s2">
                        <button type="submit" value="cadastrar" class="btn" id="btnCadastrar">Alterar</button>
                    </div>
                    <div class="col s2">
                        <button id="cancelarFunc" type="button" class="btn orange" id="btnCancelar">Cancelar</button>
                    </div>
                </div>
                <input type="hidden" value="alterarEmpresa" name="page" />
                <input type="hidden" value="<%=empresa.getId()%>" name="id_empresa" />
                <input type="hidden" name="page" value="cadastrarEmpresa"/>
            </form>   
        </div>
        <script>
            function showPassword() {
                var x = document.getElementById("senha");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }
        </script>
    </body>

    <%@include file="footer.jsp" %>

</html>
