
<%@page import="br.com.amazontrip.dao.TipoEmbarcacaoDao"%>
<%@page import="br.com.amazontrip.dao.EmpresaDao"%>
<%@page import="br.com.amazontrip.model.Embarcacao"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@page import="br.com.amazontrip.model.Empresa"%>
<%@page import="br.com.amazontrip.model.TipoEmbarcacao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Embarcações ::</title>
    </head>

    <%@include file="top.jsp"%>

    <body>

        <div class="container">
            <%
                List<TipoEmbarcacao> tipoEmbarcacao;
                List<Empresa> listEmpresa;
            %>
            <form class="col s12" name="cadastrarEmbarcacao" id="cadastrarEmbarcacao" action="../ControllerEmbarcacao" method="post">
                <div class="row">
                    <h4>Cadastrar Embarcação</h4>
                </div> 

                <div class="row">
                    <!-- Inscrição da Capitania -->
                    <div class="input-field col s4">
                        <input name="inscricao" id="inscricao" type="text" required class="validate">
                        <label for="inscricao">Inscrição da Capitania</label>
                    </div>   

                    <!-- Nome -->
                    <div class="input-field col s4">
                        <input name="nomeEmbarcacao" id="nomeEmbarcacao" type="text" required class="validate">
                        <label for="nomeEmbarcacao">Nome da Embarcação</label>
                    </div>          
                </div>


                <div class="row">
                    <!-- Tipo da Embarcação -->
                    <div class="input-field col s4">                         
                        <select id="tipoEmbarcacao" name="tipoEmbarcacao" required>
                            <option selected disabled>Selecione</option>
                            <%
                                tipoEmbarcacao = new ArrayList<TipoEmbarcacao>();
                                tipoEmbarcacao = new TipoEmbarcacaoDao().listar(TipoEmbarcacao.class);
                                for (TipoEmbarcacao tipo : tipoEmbarcacao) {
                            %>
                            <option value="<%=tipo.getId()%>"><% out.print(tipo.getTxNome()); %></option>
                            <%
                                }
                            %>
                        </select>
                        <label>Selecione o tipo da embarcação</label>
                    </div>

                    <div class="input-field col s4">   
                        <!-- Empresa Dona da Embarcação -->
                        <select id="empresa" name="empresa" required>
                            <option selected disabled>Selecione</option>
                            <%
                                listEmpresa = new ArrayList<Empresa>();
                                listEmpresa = new EmpresaDao().listar(Empresa.class);
                                for (Empresa empresa : listEmpresa) {
                            %>
                            <option value="<%=empresa.getId()%>"><% out.print(empresa.getTxNome()); %></option>
                            <%
                                }
                            %>
                        </select>
                        <label>Selecione a empresa</label>
                    </div>                    
                </div>

                <div class="row">
                    <!-- Capacidade da Embarcação -->
                    <div class="input-field col s2">
                        <input name="capacidade" id="capacidade" required type="number" class="validate">
                        <label for="capacidade">Capacidade</label> 
                    </div>
                </div>

                <div class="row">
                    <!-- TV -->
                    <div class="col s6">
                        <p><h7>A embarcação possui TV?</h7></p>
                        <p>
                            <input class="with-gap" name="tv" type="radio" id="possuiTV" value="true"  />
                            <label for="possuiTV">Sim</label>

                            <input class="with-gap" name="tv" type="radio" id="naoPossuiTV" checked value="false" />
                            <label for="naoPossuiTV">Não</label>
                        </p>
                    </div>

                    <!-- Área de Lazer -->
                    <div class="col s6">
                        <p><h7>A embarcação possui área de lazer?</h7></p>
                        <p>
                            <input class="with-gap" name="areaLazer" type="radio" id="possuiLazer" value="true" />
                            <label for="possuiLazer">Sim</label>

                            <input class="with-gap" name="areaLazer" type="radio" id="naoPossuiLazer" checked value="false"/>
                            <label for="naoPossuiLazer">Não</label>
                        </p>
                    </div>
                </div>       

                <!-- Climatização -->
                <div class="row">
                    <div class="col s6">
                        <p><h7>A embarcação possui climatização?</h7></p>

                        <p>  <input class="with-gap" name="climatizacao" type="radio" id="possuiClimatizacao" value="true" />
                            <label for="possuiClimatizacao">Sim</label>
                            <input class="with-gap" name="climatizacao" type="radio" id="naoPossuiClimatizacao" checked value="false" />
                            <label for="naoPossuiClimatizacao">Não</label>
                        </p>
                    </div>
                </div>

                <div class="row">
                    <div class="input-field col s2">
                        <input name="camarote" id="camarote" required type="number" class="validate">
                        <br> <label for="camarote"> Qtd de camarotes</label> 
                    </div>

                    <div class="input-field col s2">
                        <input name="contato" id="contato" required type="text" class="phone_with_ddd">
                        <br> <label for="contato">Contato</label> 
                    </div>
                </div>

                <div class="row col s4">
                    <div class="col s2">
                        <input type="hidden" name="page" value="cadastrarEmbarcacao"/>
                        <button type="submit" value="Cadastrar" class="btn" id="btnCadastrar">Cadastrar</button>
                    </div>
                    <div class="col s2">
                        <button type="reset" class="btn" value="Cancelar" id="btnCancelar">Cancelar</button>
                    </div>
                </div>
            </form>

        </div>

    </body>
    <%@include file="footer.jsp" %>
</html>
