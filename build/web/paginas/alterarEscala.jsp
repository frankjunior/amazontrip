<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.amazontrip.model.Escala"%>
<%@page import="br.com.amazontrip.dao.CronogramaDao"%>
<%@page import="br.com.amazontrip.model.Cronograma"%>
<%@page import="br.com.amazontrip.dao.CidadeDao"%>
<%@page import="br.com.amazontrip.dao.EmbarcacaoDao"%>
<%@page import="br.com.amazontrip.util.SemanaEnum"%>
<%@page import="br.com.amazontrip.model.Embarcacao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="br.com.amazontrip.model.Cidade"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>

        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Escala ::</title>
    </head>

    <%@include file="top.jsp"%>

    <body>
        <%

            List<Cidade> cidades = new ArrayList<Cidade>();
            SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
            List<Cronograma> cronogramas = new ArrayList<Cronograma>();
            cidades = new CidadeDao().listar(Cidade.class);
            cronogramas = new CronogramaDao().listar(Cronograma.class);
            List<Embarcacao> embarcacoes = new ArrayList<Embarcacao>();
            embarcacoes = new EmbarcacaoDao().listar(Embarcacao.class);
            Escala escala = new Escala();
            SemanaEnum dias[] = SemanaEnum.values();
            if (session.getAttribute("entity") != null) {
                escala = (Escala) session.getAttribute("entity");//recupera a escala vindo do controller RedirectServlet
            }
        %>
        <div class="container">
            <form class="col s6" name="cadastrarEscala" id="cadastrarEscala" action="../ControllerEscala" method="post">
                <h4>Adicionar Escalas ao Cronograma{ID <%=escala.getId()%>}</h4>
                <div class="row">

                    <div class="input-field col s6">
                        <select id="cronograma" required name="cronograma">
                            <option value="" disabled selected>Selecione</option>
                            <%
                                for (Cronograma cronograma : cronogramas) {
                                    if (escala.getCronograma().getId() == cronograma.getId()) {
                            %>
                            <option value="<%=cronograma.getId()%>" selected><% out.print(cronograma.getTxDescricao()); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%=cronograma.getId()%>"><% out.print(cronograma.getTxDescricao()); %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Cronograma</label>
                    </div>
                    <div class="input-field col s6">
                        <select name="escala" required>
                            <option value="" disabled selected>Selecione</option>
                            <%
                                for (Cidade cidade : cidades) {
                                    if (escala.getCidade().getId() == cidade.getId()) {
                            %>
                            <option value="<%=cidade.getId()%>" selected><% out.print(cidade.getTxNome()); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%=cidade.getId()%>"><% out.print(cidade.getTxNome()); %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Escala</label>
                    </div>
                </div>
                <div class="row">                  
                    <div class="input-field col s6">
                        <select required name="dia_semana">
                            <option value="" disabled select>Dia da semana</option>
                            <%
                                for (SemanaEnum diaSaida : dias) {
                                    if (Integer.parseInt(String.valueOf(escala.getTxDia())) == diaSaida.getValor()) {
                            %>
                            <option value="<%=diaSaida.getValor()%>" selected><% out.print(diaSaida); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%=diaSaida.getValor()%>"><% out.print(diaSaida); %></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Selecione o dia de saída</label>
                    </div>
                    <div class="input-field col s6">
                        <div>
                            <input id="hora_saida" name="data_hora" value="<%= sdf.format(escala.getTxHorario()) %>" required class="time" type="text">
                            <label for="hora_saida">Horario</label>
                        </div>
                    </div>

                    <div class="input-field col s6">
                        <div>
                            <input id="valor_padrao" name="valor_padrao" value="<%= escala.getNbValorPadrao() %>" required class="money" type="text">
                            <label for="valor_padrao">Valor Padrão</label>
                        </div>
                    </div>
                    <div class="input-field col s6">
                        <div>
                            <input id="valor_camarote" name="valor_camarote" value="<%= escala.getNbValorCamarote()%>" required class="money" type="text">
                            <label for="valor_camarote">Valor Camarote</label>
                        </div>
                    </div>

                </div> 
                <br/>

                <div class="row col s4">
                    <div class="col s2">
                        <input type="hidden" name="page" value="alterarEscala"/>
                        <input type="hidden" name="id_escala" value="<%= escala.getId() %>"/>
                        <button type="submit" value="Cadastrar" class="btn" id="btn_alterar">Alterar</button>
                    </div>
                    <div class="col s2">
                        <button type="button" class="btn orange" onclick="window.location.href = 'listandoEscala.jsp'" value="Cancelar" id="btnCancelar">Cancelar</button>
                    </div>
                </div>
            </form>
        </div>  

        <div id="modal1" class="modal bottom-sheet">
            <div class="modal-content">
                <h4>Modal Header</h4>
                <p>A bunch of text</p>
            </div>
            <div class="modal-footer">
                <a href="#!" class="modal-close waves-effect waves-green btn-flat">Fechar</a>
            </div>
        </div>
    </body>
    <%@include file="footer.jsp" %>
</html>
