
<%@page import="br.com.amazontrip.dao.CronogramaDao"%>
<%@page import="br.com.amazontrip.model.Cronograma"%>
<%@page import="br.com.amazontrip.model.Empresa"%>
<%@page import="br.com.amazontrip.model.TipoEmbarcacao"%>
<%@page import="br.com.amazontrip.dao.EmbarcacaoDao"%>
<%@page import="br.com.amazontrip.model.Embarcacao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../files/materialize/css/estilocss.css" rel="stylesheet" type="text/css"/>
        <title>:: Cronograma ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>
        <div class="container">

            <h2> Listando Cronogramas </h2>

            <%
                
                List<Cronograma> list = new ArrayList<Cronograma>();
                if (request.getParameter("busca") != null) {
                    if (request.getParameter("busca").trim().isEmpty()) {
                        list = new CronogramaDao().listar(Cronograma.class);
                    } else {
                        list = new CronogramaDao().pesquisarPorDescricao(request.getParameter("busca"));
                    }
                }
            %>
            <form action="" method="post">
                <table>
                    <tr>
                        <td width="60%"> 
                            <div class="input-field col s6">
                                <input id="busca" name="busca" type="text"  placeholder="Pesquisa" class="validate">
                            </div>
                        </td>
                        <td width="20%">
                            <button type="submit" name="btn_buscar" class="btn" id="btn_alterar">Pesquisar</button>
                        </td>
                        <td width="20%">
                            <input type="button" onclick="window.location.href = 'cadastrarCronograma.jsp'" name="btn_buscar" class="btn" value="Novo" id="btn_alterar"/>
                        </td>
                    </tr>
                </table>
                        <% if(list.size() > 0){ %>
                    <table class="striped" id="listaClientes">
                        <thead>
                            <tr>
                                <th> CÓDIGO </th>
                                <th> DESCRIÇÃO </th>
                                <th> LOC. PARTIDA </th>

                            </tr>
                        <thead>
                            <%
                                for (Cronograma cronograma : list) {
                            %>
                        <tbody>
                            <tr>
                                <td> <%= cronograma.getId()%> </td> 
                                <td width="35%"> <%= cronograma.getTxDescricao() %> </td>
                                <td> <%= cronograma.getTxLocalPartida() %> </td>


                                <td >
                                    <input type="button" onclick="window.location.href='../RedirectServlet?id_cronograma=<%=cronograma.getId()%>&pagina=listandoCronograma&action=alterar'" name="btn_alterar" value="Alterar" class="btn blue-grey" id="btn_alterar"/>
                                </td> 

                                <td>
                                    <a href=""> <button type="button" class="btn orange" value="Excluir" id="btnExcluir" name="btnExcluir">Excluir</button> </a>
                                </td>
                            </tr>  
                        </tbody>
                        <%
                            }
                        %>
                    </table> 
                    <% } %>
                <br/>
            </form> 
        </div>
    </body>

    <%@include file="footer.jsp" %>
</html>

