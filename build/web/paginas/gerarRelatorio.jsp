
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.amazontrip.dao.PassagemDao"%>
<%@page import="br.com.amazontrip.model.Passagem"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.amazontrip.dao.ClienteDao"%>
<%@page import="java.util.List"%>
<%@page import="br.com.amazontrip.model.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="files/materialize/css/estilocss.css" rel="stylesheet" type="text/css"/>
        <title>:: Cliente ::</title>
    </head>

    <%@include file="top.jsp" %>
    <%
        int tipo = 0;
        if (request.getParameter("tipo") != null) {
            tipo = Integer.parseInt(request.getParameter("tipo"));//verifica o tipo de relatório (1 = Todas as passagens; 2 = Por datas)
        }
    %>
    <body>
        <div class="container">            
            <h4>Relatório de Passagens</h4>
            <div class="row">   
                <form action="" method="get">
                    <div class="input-field col s2">
                        <select onchange="submit()" required name="tipo">
                            <option disabled select>Escolha</option>  
                            <% if (tipo <= 1) { %>
                            <option value="1" selected><% out.print("Todas"); %></option>
                            <option value="2"><% out.print("Por Período"); %></option>
                            <%} else if (tipo == 2) {%>
                            <option value="1"><% out.print("Todas"); %></option>
                            <option value="2" selected><% out.print("Por Período"); %></option>
                            <%}%>
                        </select>
                        <label>Selecione</label>
                    </div>
                </form>
                <% if (tipo > 0) {%>
                <form action="../RelatorioController" method="get">
                    <% if (tipo == 2) { %>
                    <div class="input-field col s2">
                        <div>
                            <input id="hora_saida" name="data_inicio" required class="date" type="text">
                            <label for="hora_saida">Data Inicial</label>
                        </div>
                    </div>

                    <div class="input-field col s2">
                        <div>
                            <input id="valor_padrao" name="data_fim" required class="date" type="text">
                            <label for="valor_padrao">Data Final</label>
                        </div>
                    </div>
                    <%}%>
                    <div class="input-field col s2">
                        <div class="col s2">
                            <input type="hidden" value="<%= request.getParameter("tipo")%>" name="tipo"/>
                            <input type="hidden" value="gerarRelatorio" name="pagina"/>
                            <button type="submit" value="Cadastrar" class="btn" id="btnCadastrar">Imprimir</button>
                        </div>
                    </div>
                </form>
                <%}%>
            </div> 
            <br/>
        </div>
    </body>

    <%@include file="footer.jsp" %>
</html>

