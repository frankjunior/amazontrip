

<%@page import="br.com.amazontrip.model.Funcionario"%>
<%@page import="br.com.amazontrip.dao.EmpresaDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="java.util.List"%>
<%@page import="br.com.amazontrip.model.Empresa"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta charset=UTF-8">
        <title>:: Alterar Funcionário ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>

        <div class="container">
            <%
                Funcionario funcionario = new Funcionario();
                List<Empresa> listEmpresa = new ArrayList<Empresa>();
                listEmpresa = new EmpresaDao().listar(Empresa.class);
                if (session.getAttribute("entity") != null) {
                    funcionario = (Funcionario) session.getAttribute("entity");//recupera o funcionário vindo do controller RedirectServlet
                }
            %>
            <h4>Alteração de Funcionário {ID: <%=funcionario.getId()%>}</h4>
            <form class="col s12" name="alterarFuncionario" id="cadastrarFuncionario" action="../ControllerFuncionario" method="post">

                <div class="row">
                    <!-- CPF -->
                    <div class="input-field col s6">
                        <input id="cpf" name="cpf" type="text" value="<%=funcionario.getTxCpf()%>" class="cpf">
                        <label for="cpf">CPF</label>
                    </div>
                    <!-- Nome -->
                    <div class="input-field col s6">
                        <input id="nome" name="nome" type="text" value="<%=funcionario.getTxNome()%>" class="validate">
                        <label for="nome">Nome</label>
                    </div>    
                    <!-- Senha -->

                </div>

                <div class="row"> 
                    <div class="input-field col s6">
                        <input id="login" name="login" type="text" value="<%=funcionario.getUsuario().getTxLogin()%>" class="validate" maxlength="20">
                        <label for="login"> Login </label>
                    </div>
                    <div class="input-field col s6">
                        <input id="senha" name="senha" type="password" value="<%=funcionario.getUsuario().getTxSenha()%>" class="validate">
                        <label for="senha">Senha</label>
                        <img width="20" height="20" src="../files/imagens/showPass.png" onclick="showPassword()" title="exibir senha"/>
                    </div>
                </div>
                <div class="row"> 
                    <div class="input-field col s6">                        
                        <select id="empresa" name="empresa">
                            <option value="" disabled selected>Selecione</option>
                            <%
                                for (Empresa empresa : listEmpresa) {
                            %>
                            <% if (funcionario.getEmpresa().getId() == empresa.getId()) {%>
                            <option value="<%= empresa.getId()%>" selected><% out.print(empresa.getTxNome()); %></option>
                            <%
                            } else {
                            %>
                            <option value="<%= empresa.getId()%>"><% out.print(empresa.getTxNome()); %></option>
                            <%
                                }
                            %>
                            <%
                                }
                            %>
                        </select>
                        <label>Selecione a empresa:</label>
                    </div> 

                </div>

                <script>
                    function msgCadFuncionario()
                    {
                        alert("Funcionário cadastrado com sucesso!");
                    }
                </script>

                <br/>
                <div class="row col s4">
                    <div class="col s2">
                        <input type="hidden" value="alterarFuncionario" name="page" />
                        <input type="hidden" value="<%=funcionario.getId()%>" name="id_funcionario" />
                        <button type="submit" value="alterar" name="btn_alterar" class="btn" id="btnAlterar">Alterar</button>
                    </div>
                    <div class="col s2">
                        <button class="btn orange" type="button" value="cancelar" id="btnCancelar" onclick="window.location.href = 'listandoFuncionario.jsp'" >Cancelar</button>
                    </div>
                </div>
                <input type="hidden" name="page" value="cadastrarFuncionario"/>
            </form>          

        </div>
        <script>
            function showPassword() {
                var x = document.getElementById("senha");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }
        </script>
        
    </body>

    <br />
    <br />
    <br />

    <%@include file="footer.jsp" %>

</html>
