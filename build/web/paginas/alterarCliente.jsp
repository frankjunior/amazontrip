

<%@page import="br.com.amazontrip.dao.TipoClienteDao"%>
<%@page import="br.com.amazontrip.model.TipoCliente"%>
<%@page import="java.util.List"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.amazontrip.model.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Alterar Cliente ::</title>
    </head>
    
    <%@include file="top.jsp" %>
    <body>
        <div class="container">
            <%
                Cliente cliente = new Cliente();
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                if (session.getAttribute("entity") != null) {
                    cliente = (Cliente) session.getAttribute("entity");//recupera o usuário vindo do controller RedirectServlet
                }
            %>
            <h2> Alteração de Cliente {ID: <%=cliente.getId()%>}</h2>
            <form class="col s12" name="form_alterar_cliente" id="form_alterar_cliente" action="../ControllerCliente" method="post">
                <fieldset> 
                    <legend> Alterar cliente </legend>


                    <div class="row">
                        <!-- Login -->
                        <div class="input-field col s6">
                            <input id="login" name="login" type="text" class="validate" value="<%=cliente.getUsuario().getTxLogin()%>" >
                            <label for="login">Login</label>
                        </div>

                        <!-- Senha -->                        
                        <div class="container">
                            <div class="input-field col s6">
                                <input id="senha" name="senha" type="password" class="validate" value="<%=cliente.getUsuario().getTxSenha()%>" >                            
                                <label for="senha">Senha</label>
                                <img width="20" height="20" src="../files/imagens/showPass.png" onclick="showPassword()" title="exibir senha"/>
                            </div>
                        </div>

                    </div>
                    <div class="row">
                        <!-- Nome -->
                        <div class="input-field col s6">
                            <input id="nome" name="nome" type="text" class="validate" value="<%=cliente.getTxNome()%>" >
                            <label for="nome">Nome</label>
                        </div>

                        <!-- RG -->
                        <div class="input-field col s6">
                            <input id="rg" name="rg" type="text" class="rg" value="<%=cliente.getTxRg()%>" >
                            <label for="rg">RG</label>
                        </div>
                    </div>

                    <div class="row">                     
                        <!-- CPF -->
                        <div class="input-field col s6">
                            <input id="cpf" name="cpf" type="text" class="cpf" value="<%=cliente.getTxCpf()%>" >
                            <label for="cpf">CPF</label>
                        </div>

                        <!-- Telefone Pessoal -->
                        <div class="input-field col s6">
                            <input  id="telefonePessoal" name="telefonePessoal" type="tel" class="phone_with_ddd" value="<%=cliente.getTxTelefonePessoal()%>" >
                            <label for="telefonePessoal">Telefone Pessoal</label>
                        </div>        
                    </div>        

                    <div class="row">
                        <!-- Telefone Emergencial -->
                        <div class="input-field col s6">
                            <input  id="telefoneEmergencial" name="telefoneEmergencial" type="tel" class="phone_with_ddd" value="<%=cliente.getTxTelefoneEmergencial()%>" >
                            <label for="telefoneEmergencial">Telefone Emergencial</label>
                        </div>  

                        <!-- Data de Nascimento -->
                        <div class="input-field col s6">
                            <input id="dataNascimento" name="dataNascimento" type="text" class="date" value="<%= sdf.format(cliente.getDtNascimento())%>" >
                            <label for="dataNascimento">Data Nascimento</label>
                        </div>  

                        <!-- Data de Nascimento -->
                        <div class="input-field col s6">
                            <select id="tipoCliente" name="tipoCliente">
                                <option value="0" disabled>Selecione</option>
                                <%
                                    List<TipoCliente> list = new TipoClienteDao().listar(TipoCliente.class);
                                    for (TipoCliente tipo : list) {
                                %>
                                <% if (cliente.getTipoCliente().getId() == tipo.getId()) {%>
                                <option value="<%= tipo.getId()%>" selected><% out.print(tipo.getTxNome()); %></option>
                                <%
                                } else {
                                %>
                                <option value="<%= tipo.getId()%>"><% out.print(tipo.getTxNome()); %></option>
                                <%
                                    }
                                %>

                                <%
                                    }
                                %>
                            </select>
                            <label for="tipoCliente">Tipo Usuário</label>
                        </div>            

                    </div>

                    <br />

                    <div class="row col s4">
                        <div class="col s2">
                            <input type="hidden" value="alterarCliente" name="page" />
                            <input type="hidden" value="<%=cliente.getId()%>" name="id_cliente" />
                            <button type="submit" value="alterar" name="btn_alterar" class="btn" id="btnAlterar">Alterar</button>
                        </div>
                        <div class="col s2">
                            <button class="btn orange" type="button" value="cancelar" id="btnCancelar" onclick="window.location.href = 'listandoCliente.jsp'" >Cancelar</button>
                        </div>
                    </div>
                </fieldset>
            </form>

        </div>
        <script>
            function showPassword() {
                var x = document.getElementById("senha");
                if (x.type === "password") {
                    x.type = "text";
                } else {
                    x.type = "password";
                }
            }
        </script>
    </body>

    <%@include file="footer.jsp" %>
</html>

