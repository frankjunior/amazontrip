
<%@page import="br.com.amazontrip.util.SemanaEnum"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.amazontrip.dao.EscalaDao"%>
<%@page import="br.com.amazontrip.model.Escala"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.amazontrip.dao.ClienteDao"%>
<%@page import="java.util.List"%>
<%@page import="br.com.amazontrip.model.Cliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="../files/materialize/css/estilocss.css" rel="stylesheet" type="text/css"/>
        <title>:: Escala ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>
        <div class="container">

            <h2> Listando Escalas </h2>

            <%
                List<Escala> list = new ArrayList<Escala>();
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                if (request.getParameter("busca") != null) {
                    if (request.getParameter("busca").trim().isEmpty()) {
                        list = new EscalaDao().listar(Escala.class);
                    } else {
                        list = new EscalaDao().pesquisarPorCronograma(request.getParameter("busca"));
                    }
                }
            %>
            <form action="" method="post">
                <table>
                    <tr>
                        <td width="60%"> 
                            <div class="input-field col s6">
                                <input id="busca" name="busca" type="text"  placeholder="Pesquisa" class="validate">
                            </div>
                        </td>
                        <td width="20%">
                            <button type="submit" name="btn_buscar" onclick="showTable('resultList');" class="btn" id="btn_alterar">Pesquisar</button>
                            <input type="hidden" id="resultList" value="<%= list.size()%>"/>
                        </td>
                        <td width="20%">
                            <input type="button" onclick="window.location.href = 'cadastrarEscala.jsp'" name="btn_buscar" class="btn" value="Novo" id="btn_alterar"/>
                        </td>
                    </tr>
                </table>
                <% if (list.size() > 0) { %>
                    <table class="striped" id="listaClientes">
                        <thead>
                            <tr>
                                <th> CÓDIGO </th>
                                <th> ESCALA </th>
                                <th> CRONOGRAMA </th>
                                <th> DIA/HORARIO </th>

                            </tr>
                        <thead>
                            <%
                                for (Escala escala : list) {
                            %>
                        <tbody>
                            <tr>
                                <td> <%= escala.getId()%> </td> 
                                <td width="30%"> <%= escala.getCidade().getTxNome()%> </td>
                                <td width="20%"> <%= escala.getCronograma().getTxDescricao()%> </td>
                                <td> <%= SemanaEnum.getValor(escala.getTxDia()) %> / <%= sdf.format(escala.getTxHorario())%> </td>


                                <td >
                                    <input type="button" onclick="window.location.href = '../RedirectServlet?id_escala=<%=escala.getId()%>&pagina=listandoEscala&action=alterar'" name="btn_alterar" value="Alterar" class="btn blue-grey center" id="btn_alterar"/>
                                </td>
                            </tr>  
                        </tbody>
                        <%
                            }
                        %>
                    </table>
                <% }%>
                <br/>
            </form> 
        </div>
    </body>

    <%@include file="footer.jsp" %>
</html>

