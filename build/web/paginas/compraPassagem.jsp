<%@page import="br.com.amazontrip.dao.PassagemDao"%>
<%@page import="br.com.amazontrip.model.Passagem"%>
<%@page import="br.com.amazontrip.dao.FuncionarioDao"%>
<%@page import="br.com.amazontrip.model.Funcionario"%>
<%@page import="java.math.BigDecimal"%>
<%@page import="br.com.amazontrip.dao.ClienteDao"%>
<%@page import="br.com.amazontrip.model.Cliente"%>
<%@page import="java.text.SimpleDateFormat"%>
<%@page import="br.com.amazontrip.dao.CronogramaDao"%>
<%@page import="br.com.amazontrip.model.Cronograma"%>
<%@page import="br.com.amazontrip.util.SemanaEnum"%>
<%@page import="br.com.amazontrip.dao.CidadeDao"%>
<%@page import="br.com.amazontrip.model.Cidade"%>
<%@page import="br.com.amazontrip.dao.EscalaDao"%>
<%@page import="java.util.ArrayList"%>
<%@page import="br.com.amazontrip.model.Escala"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Compra de Passagem ::</title>
    </head>

    <%@include file="top.jsp" %>


    <%
        List<Cidade> origens = new ArrayList<Cidade>();
        origens = new CidadeDao().listar(Cidade.class);
        List<Escala> destinos = new ArrayList<Escala>();
        List<Escala> cronoEscala = new ArrayList<Escala>();
        List<Passagem> qtdPassagens = new ArrayList<Passagem>();
        SimpleDateFormat sdfTime = new SimpleDateFormat("HH:mm");
        SemanaEnum dias[] = SemanaEnum.values();
        Cidade tmpCidade = new Cidade();
        Funcionario funcionario = null;
        Cliente cliente = null;
        Usuario usuario = null;
        Cronograma tmpCrono = new Cronograma();
        Escala tmpEscala = new Escala();
        int origem = 0;
        int destino = 0;
        int crono = 0;
        int tipoPassagem = 0;
        boolean cronoSelected = false;
        if (session.getAttribute("userEntity") != null) {
            usuario = (Usuario) session.getAttribute("userEntity");
            if (usuario.getTipoUsuario().getId() == 4) {
                cliente = new ClienteDao().clientePeloUsuario(usuario);
            } else if (usuario.getTipoUsuario().getId() == 3 || usuario.getTipoUsuario().getId() == 1) {
                funcionario = new FuncionarioDao().funcionarioPeloUsuario(usuario);
            }

        }
        if (request.getParameter("cliente_busca") != null) {
            cliente = new ClienteDao().buscarPeloCPF(request.getParameter("cliente_busca"));
        }
        if (request.getParameter("origem") != null) {
            tmpCidade = new Cidade();
            origem = Integer.parseInt(request.getParameter("origem"));
            tmpCidade = new CidadeDao().buscarPorId(origem);
            if (origem > 0) {
                destinos = new EscalaDao().cidadesComEscala(tmpCidade);
            }
        }
        if (request.getParameter("destino") != null) {
            tmpCrono = new Cronograma();
            destino = Integer.parseInt(request.getParameter("destino"));
            if (destino > 0) {
                tmpCrono = new CronogramaDao().buscarPorId(destino);
                cronoEscala = new EscalaDao().cidadesComEscalaNoCronograma(tmpCrono, tmpCidade);

            }
        }
        if (request.getParameter("cronograma") != null) {
            tmpCrono = new Cronograma();
            crono = Integer.parseInt(request.getParameter("cronograma"));
            tipoPassagem = 0;
            if (crono > 0) {
                tmpCrono = new CronogramaDao().buscarPorId(crono);
                tmpEscala = new EscalaDao().getEscalaCronogramaCidade(origem, crono).get(0);
                qtdPassagens = new PassagemDao().quantidadePassagemnsPorEscala(tmpEscala);
                cronoSelected = true;
            } else {
                cronoSelected = false;
            }
        }
        if (request.getParameter("tipo_passagem") != null) {
            tipoPassagem = Integer.parseInt(request.getParameter("tipo_passagem"));
        }
    %>
    <body>
        <div class="row">
            <form class="col s6 offset-m3" action="" method="get">
                <h4>Compra de passagens</h4>
                <div class="row">
                    <!-- buscar cliente pelo cpf -->
                    <% if (usuario != null && usuario.getTipoUsuario().getId() == 3 || usuario.getTipoUsuario().getId() == 1) {%>
                    <div class="row col s12">
                        <div class="col s8">
                            <input id="cliente_busca" placeholder="Informe o CPF do Cliente" value="<%= request.getParameter("cliente_busca") != null ? request.getParameter("cliente_busca") : ""%>" name="cliente_busca" type="text" class="text" >

                        </div>                        
                        <div class="col s4">
                            <button type="submit" class="btn deep-orange small" value="buscar" id="btnBuscar">Buscar</button>
                        </div>
                    </div>
                    <% if (cliente == null && request.getParameter("cliente_busca") != null) { %>
                    <div class="red-text background alert-danger">
                        <strong>Usuario nao encontrado</strong>
                    </div>
                    <%}%>
                    <% if (cliente != null) {%>
                    <div class="row col s7">
                        <div class="col s2">
                            <span class="text">Cliente</span>
                        </div>                        
                        <div class="col s8">
                            <span class="text"><%= cliente.getTxNome()%></span>
                        </div>

                    </div>
                    <%}%>
                    <%}%>
                    <% if (cliente != null || (usuario != null && usuario.getTipoUsuario().getTxNome().equals("Cliente"))) { %>
                    <div class="input-field col s6">
                        <select required onchange="submit();" name="origem">
                            <option value="" disabled selected>Escolha a origem</option>
                            <%
                                for (Cidade from : origens) {
                                    if (origem == from.getId()) {
                            %>
                            <option value="<%= from.getId()%>" selected><%= from.getTxNome()%></option>
                            <%
                            } else {
                            %>
                            <option value="<%= from.getId()%>"><%= from.getTxNome()%></option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Selecione a origem:</label>
                    </div>
                    <%}%>
                    <% if (destinos.size() > 0) { %>
                    <div class="input-field col s6">
                        <select required onchange="submit();" name="destino">
                            <option value="" disabled selected>Escolha o destino</option>
                            <%
                                for (Escala to : destinos) {
                                    if (destino == to.getCronograma().getId()) {
                            %>
                            <option value="<%= to.getCronograma().getId()%>" selected><%= to.getCronograma().getCidadeByIdCidadeDestino().getTxNome()%> (<%= to.getCronograma().getTxDescricao()%>)</option>
                            <%
                            } else {
                            %>
                            <option value="<%= to.getCronograma().getId()%>"><%= to.getCronograma().getCidadeByIdCidadeDestino().getTxNome()%> (<%= to.getCronograma().getTxDescricao()%>)</option>
                            <%
                                    }
                                }
                            %>
                        </select>
                        <label>Selecione o destino (cronograma)</label>
                    </div>
                    <% }
                        if (cronoEscala.size() > 0) { %>
                    <div class="input-field col s6">
                        <select required onchange="submit();" name="cronograma">

                            <option value="" disabled selected>Escolha a embarcação</option>
                            <%
                                for (Escala cronos : cronoEscala) {
                                    if (crono == cronos.getCronograma().getId()) {
                            %>
                            <option value="<%= cronos.getCronograma().getId()%>" selected><%= cronos.getCronograma().getEmbarcacao().getTxNome()%> | <%= cronos.getCronograma().getEmbarcacao().getNbCapacidade() - qtdPassagens.size()%> passagens disponiveis</option>
                            <%
                            } else {
                            %>
                            <option value="<%= cronos.getCronograma().getId()%>"><%= cronos.getCronograma().getEmbarcacao().getTxNome()%> | <%= cronos.getCronograma().getEmbarcacao().getNbCapacidade() - qtdPassagens.size()%> passagens disponiveis</option>
                            <%
                                    }
                                }
                            %>
                        </select>                        
                        <label>Selecione a embarcação</label>

                    </div>
                    <!-- Dia semana -->
                    <% }
                        if (cronoSelected) {%>
                    <div class="input-field col s6">
                        <input disabled id="saida_chegada" name="saida_chegada" type="text" value="<%= SemanaEnum.getValor(tmpEscala.getTxDia())%>/<%= sdfTime.format(tmpEscala.getTxHorario())%> | <%= SemanaEnum.getValor(tmpEscala.getCronograma().getNbDiaSemanaChegada())%>/<%= sdfTime.format(tmpEscala.getCronograma().getNbHorarioChegada())%>" />
                        <label><% out.print("Saída | Chegada"); %></label>
                    </div>
                    <%
                        }
                        if (cronoSelected) {
                    %>
                    <div class="row">
                        <!-- Tipo da passagem -->
                        <% }
                            if (cronoSelected) {%>
                        <div class="input-field col s6">
                            <select onchange="submit();" required name="tipo_passagem">
                                <option value="0" disabled selected><% out.print("Selecione");%></option>
                                <% if (tipoPassagem == 1) { %>
                                <option value="1" selected><% out.print("Padrão");%></option>
                                <option value="2"><% out.print("Camarote");%></option>
                                <%} else if (tipoPassagem == 2) {%>
                                <option value="1" ><% out.print("Padrão");%></option>
                                <option value="2" selected><% out.print("Camarote");%></option>
                                <%} else {%>
                                <option value="1"><% out.print("Padrão");%></option>
                                <option value="2"><% out.print("Camarote");%></option>
                                <%}%>
                            </select>
                            <label>Selecine o tipo da passagem</label>
                        </div>
                        <% if (request.getParameter("tipo_passagem") != null) { %>
                        <!-- valor passagem -->
                        <div class="input-field col s6">
                            <% if (tipoPassagem == 1) {%>
                            <input disabled id="valor" name="valor" type="text" class="money" value="<%= cliente.getTipoCliente().getId() == 2 ? (tmpEscala.getNbValorPadrao().divide(new BigDecimal(2))) : tmpEscala.getNbValorPadrao()%>" />
                            <%} else if (tipoPassagem == 2) {%>
                            <input disabled id="valor" name="valor" type="text" class="money" value="<%= cliente.getTipoCliente().getId() == 2 ? (tmpEscala.getNbValorCamarote().divide(new BigDecimal(2))) : tmpEscala.getNbValorCamarote()%>" />
                            <%} else {%>
                            <input disabled id="valor" name="valor" type="text" class="money" value="<%= cliente.getTipoCliente().getId() == 2 ? (tmpEscala.getNbValorPadrao().divide(new BigDecimal(2))) : tmpEscala.getNbValorPadrao()%>" >
                            <%}%>
                            <label for="valor">Valor</label>
                            <% if (cliente.getTipoCliente().getId() == 2) { %>
                            <span class="helper-text small green-text">Desconto concedido de 50% para idoso</span>
                            <%}%>
                        </div>
                        <%}%>
                    </div>
                    <% if (request.getParameter("tipo_passagem") != null) { %>
                    <div class="row col s8">
                        <div class="col s4">
                            <% if (funcionario != null) {%>
                            <a onclick="JavaScript:timedRedirect()"><button type="button" name="cadastrar" onclick="window.location.href = '../ControllerPassagem?escala=<%= tmpEscala.getId()%>&page=compraPassagem&tipo_passagem=<%= request.getParameter("tipo_passagem")%>&id_cliente=<%= cliente.getId()%>&id_funcionario=<%= funcionario.getId()%>'" value="cadastrar" class="btn" id="btnCadastrar">Cadastrar</button></a>
                            <%} else {%>
                            <a onclick="JavaScript:timedRedirect()" > <button type="button" name="cadastrar" onclick="window.location.href = '../ControllerPassagem?escala=<%= tmpEscala.getId()%>&page=compraPassagem&tipo_passagem=<%= request.getParameter("tipo_passagem")%>&id_cliente=<%= cliente.getId()%>'" value="cadastrar" class="btn" id="btnCadastrar">Cadastrar</button></a>
                            <%}%>
                        </div>
                        <div class="col s4">
                            <button type="button" onclick="window.location.href = 'compraPassagem.jsp'" class="btn deep-orange" value="cancelar" id="btnCancelar">Cancelar</button>
                        </div>
                    </div> 
                        
                    <%
                            }
                        }
                    %>
                </div>  
                <div class="green-text background alert-danger" id="delayMsg"></div>
            </form>
        </div>
    </body>
    <script>
        redirectTime = "1500";
        redirectURL = "../index.jsp";
        
        function timedRedirect() {
            document.getElementById('delayMsg').innerHTML = "<strong>Imprimindo Passagem...</strong>";            
            setTimeout("location.href = redirectURL;", redirectTime);
        }
    </script>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <br/>
    <%@include file="footer.jsp" %>
</html>
