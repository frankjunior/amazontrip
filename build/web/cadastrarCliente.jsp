<%@page import="java.util.List"%>
<%@page import="br.com.amazontrip.dao.TipoClienteDao"%>
<%@page import="br.com.amazontrip.model.TipoCliente"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Cadastro de Clientes ::</title>
        <link href="files/dist/jquery.mask.js" type="text/javascript" >   
        <link href=files/dist/jquery.mask.min.js" type="text/javascript" >
    </head>

    <%@include file="top.jsp"%>
    <body>
        <div class="container">
            <form class="col s12" name="cadastrarCliente" id="cadastrarCliente" action="ControllerCliente" method="post">
                <div class="row">
                    <h4>Registrar-se</h4>
                </div> 

                <div class="row">
                    <!-- Login -->
                    <div class="input-field col s4">
                        <input id="login" name="login" required type="text" class="validate">
                        <label for="login">Login</label>
                    </div>

                    <!-- Senha -->
                    <div class="input-field col s4">
                        <input id="senha" name="senha" required type="password" class="validate">
                        <label for="senha">Senha</label>
                    </div>
                </div>
                <div class="row">
                    <!-- Nome -->
                    <div class="input-field col s4">
                        <input id="nome" name="nome" type="text" required class="validate">
                        <label for="nome">Nome</label>
                    </div>

                    <!-- RG -->
                    <div class="input-field col s4">
                        <input id="rg" name="rg" type="text" required class="rg">
                        <label for="rg">RG</label>
                    </div>
                </div>

                <div class="row">                     
                    <!-- CPF -->
                    <div class="input-field col s4">
                        <input id="cpf" name="cpf" type="text" required class="cpf">
                        <label for="cpf">CPF</label>
                    </div>


                    <!-- Data de Nascimento -->
                    <div class="input-field col s4">
                        <input id="dataNascimento" required name="dataNascimento" type="text" class="date">
                        <label for="dataNascimento">Data de Nascimento</label>
                    </div> 
                </div>        

                <div class="row">
                    <!-- Telefone Emergencial -->
                    <div class="input-field col s4">
                        <input id="telefoneEmergencial" required name="telefoneEmergencial" type="tel" class="phone_with_ddd">
                        <label for="telefoneEmergencial">Telefone Emergencial</label>
                    </div> 
                    <!-- Telefone Pessoal -->
                    <div class="input-field col s4">
                        <input id="telefonePessoal" name="telefonePessoal" required type="text" class="phone_with_ddd">
                        <label for="telefonePessoal">Telefone Pessoal</label>
                    </div> 

                </div> 

                <div class="row">

                    <div class="input-field col s4">                        
                        <select id="tipoCliente" name="tipoCliente">
                            <option value="0" disabled>Selecione</option>
                            <%
                                List<TipoCliente> list = new TipoClienteDao().listar(TipoCliente.class);
                                for (TipoCliente tipo : list) {
                            %>
                            <option value="<%= tipo.getId()%>"><% out.print(tipo.getTxNome()); %></option>
                            <%
                                }
                            %>
                        </select>
                        <label>Selecione o tipo do Cliente</label>
                    </div> 

                </div>

                <br/>

                <div class="row col s4">
                    <div class="col s2">
                        <button type="submit" name="cadastrar" value="cadastrar" class="btn" id="btnCadastrar">Cadastrar</button>
                    </div>
                    <div class="col s2">
                        <button type="reset" onclick="return confirm('Deseja cancelar o cadastro do Cliente?');" class="btn orange" value="cancelar" id="btnCancelar">Cancelar</button>
                    </div>
                </div> 
                <input type="hidden" name="page" value="cadastrarCliente"/>
            </form>

        </div>
    </body>
    <%@include file="footer.jsp" %>
</html>
