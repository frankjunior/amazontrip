<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>:: Cadastro de Empresa ::</title>
    </head>

    <%@include file="top.jsp" %>

    <body>
        <div class="container">

            <h4>Cadastro de Empresa</h4>

            <form class="col s12" name="cadastrarEmpesa" id="cadastrarEmpresa" action="ControllerEmpresa" method="post">

                <div class="row">
                    <!-- Nome -->
                    <div class="input-field col s6">
                        <input id="nome" name="nome" type="text" maxlength="50" class="validate">
                        <label for="nome">Nome</label>
                    </div>

                    <!-- CNPJ -->
                    <div class="input-field col s4">
                        <input id="cnpj" name="cnpj" type="text" maxlength="18" class="cnpj">
                        <label for="cnpj">CNPJ</label>
                    </div>
                </div>

                <div class="row">
                    <!-- Proprietário -->
                    <div class="input-field col s6">
                        <input id="proprietario" name="proprietario" type="text" maxlength="50" class="validate">
                        <label for="proprietario">Proprietário</label>
                    </div>   

                    <!-- Telefone -->
                    <div class="input-field col s4">
                        <input id="telefone" name="telefone" type="tel" maxlength="17" class="phone_with_ddd">
                        <label for="telefone">Telefone</label>
                    </div> 
                </div> 
                
                <div class="row"> 
                    <div class="input-field col s3">
                        <input id="login" name="login" type="text" class="validate" maxlength="20">
                        <label for="login"> Login </label>
                    </div>
                    <div class="input-field col s3">
                        <input id="senha" name="senha" type="password" class="validate">
                        <label for="senha">Senha</label>
                    </div>
                </div>

                <div class="row col s4">
                    <div class="col s2">
                        <button type="submit" value="cadastrar" class="btn" id="btnCadastrar">Cadastrar</button>
                    </div>
                    <div class="col s2">
                        <button id="cancelarFunc" type="reset" class="btn orange" value="cancelar" id="btnCancelar">Cancelar</button>
                    </div>
                </div>
                <input type="hidden" name="page" value="cadastrarEmpresa"/>
            </form>   
        </div>

    </body>

    <%@include file="footer.jsp" %>

</html>
